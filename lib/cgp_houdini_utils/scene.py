"""
python objects and management functions to manipulate a variety of entities in a houdini scene
such as nodes, parms ...
"""


class Node(object):
    """node object that manipulates any kind of node
    """

    # INIT #

    def __init__(self, name):
        """Node class initialization

        :param name: name of the node
        :type name: str
        """

        pass

    def __getattr__(self, attribute):
        """override getattr

        :param attribute: the attribute to get
        :type attribute: classAttribute
        """

        pass

    # COMMANDS #

    def houNode(self):
        """the houNode of the node

        :return: the houNode
        :rtype: :class:`hou.Node`
        """

        pass

    def parm(self, parm_):
        """the parm of the node

        :param parm_: the parm to get
        :type: str

        :return: the parm of the node
        :rtype: `:class: rdo_houdini_utils.scene.Parm`
        """

        pass


class Parm(object):
    """node object that manipulates any kind of parm
    """

    # INIT #

    def __init__(self, node, parm):
        """Parm class initialization

        :param node: node on which the parm exists
        :type node: str

        :param parm: name of the parm
        ;param parm: str
        """

        pass

    def __getattr__(self, attribute):
        """override getattr

        :param attribute: the attribute to get
        :type attribute: any
        """

        pass

    # COMMANDS #

    def evalAsLiteral(self):
        """eval the attribute raw value as a literal

        :return: the evaluated literal
        :rtype: literal
        """

        pass

    def houParm(self):
        """the houParm of the parm

        :return: the houParm
        :rtype: :class:`hou.Parm`
        """

        pass

    def node(self):
        """the node of the parm

        :return: the node of the parm
        :rtype: `:class: rdo_houdini_rig_utils.scene.Node`
        """

        pass


class Scene(object):
    """scene object that manipulates a live scene
    """

    # INIT #

    def __getattr__(self, attribute):
        """override getattr

        :param attribute: the attribute to get
        :type attribute: str

        :return: the attr value
        :rtype: any
        """

        pass

    # STATIC COMMANDS #

    @staticmethod
    def file_():
        """the file of the scene

        :return: the file of the scene
        :rtype: :class:`cgp_generic_utils.files.File`
        """

        pass

    # COMMANDS #

    def reopen(self, force=False):
        """reopen the scene file

        :param force: defines whether or not the command will force the reopening of the scene
        :type force: bool
        """

        pass


def node(name):
    """the node object from a node name

    :param name: the name of the node
    :type name: str

    :return: the node object
    :rtype: `:class: rdo_houdini_utils.scene.Node`
    """

    pass


def parm(nodeName, parmName):
    """the parm object from an attribute full name

    :param nodeName: the name of node of the parm
    :type nodeName: str

    :param parmName: the name of the parm
    :type parmName: str

    :return: the parm object
    :rtype: `:class: rdo_houdini_utils.scene.Parm`
    """

    pass
