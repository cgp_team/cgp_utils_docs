"""
subclassed maya api objects and custom api objects
"""


import maya.api.OpenMaya


class AnimKey(object):
    """animation key object that manipulates a key of an animCurve stored in an animCurve node
    """

    # INIT #

    def __init__(self, node, frame, value):
        """AnimKey class initialization

        :param node: animCurve node where the animation key exists
        :type node: str or :class:`cgp_maya_utils.scene.Node`

        :param frame: frame of the animation key
        :type frame: float

        :param value: value of the animation key
        :type value: float
        """

        pass

    def __repr__(self):
        """the representation of the animation key

        :return: the representation of the animation key
        :rtype: str
        """

        pass

    # OBJECT COMMANDS #

    @classmethod
    def create(cls, node, frame=None, value=None, inTangentType=None, outTangentType=None, **__):
        """create an animation key

        :param node: animCurve node where the animation key exists
        :type node: str or :class:`cgp_maya_utils.scene.Node`

        :param frame: frame of the animation key - currentFrame if Nothing specified
        :type frame: float

        :param value: value of the animation key - key is inserted if nothing is specified
        :type value: float

        :param inTangentType: inTangent type of the animation key - default is ``cgp_maya_utils.constants.TangentType.AUTO``
        :type inTangentType: str

        :param outTangentType: outTangent type of the animation key - default is ``cgp_maya_utils.constants.TangentType.AUTO``
        :type outTangentType: str

        :return: the created animation key
        :rtype: :class:`cgp_maya_utils.api.AnimKey`
        """

        pass

    # COMMANDS #

    def data(self):
        """data necessary to store the animation key on disk and/or recreate it from scratch

        :return: the data of the animation key
        :rtype: dict
        """

        pass

    def inTangentType(self):
        """the inTangent type of the animation key

        :return: the inTangent type of the animation key
        :rtype: str
        """

        pass

    def frame(self):
        """the frame of the animation key

        :return: the frame of the animation key
        :rtype: float
        """

        pass

    def outTangentType(self):
        """the outTangent type of the animation key

        :return: the outTangent type of the animation key
        :rtype: str
        """

        pass

    def value(self):
        """the value of the animation key

        :return: the value of the animation key
        :rtype: float
        """

        pass


class MayaObject(maya.api.OpenMaya.MObject):
    """MObject with custom functionalities
    """

    # INIT #

    def __init__(self, node):
        """MayaObject class initialization

        :param node: node to get the MayaObject from
        :type node: str or :class:`cgp_maya_utils.scene.Node`
        """

        pass

    def __repr__(self):
        """the representation of the maya object

        :return: the representation of the maya object
        :rtype: str
        """

        pass


class TransformationMatrix(maya.api.OpenMaya.MTransformationMatrix):
    """MTransformationMatrix with custom functionalities
    """

    # INIT #

    def __init__(self, matrix=None, translate=None, rotate=None, scale=None, shear=None, rotateOrder=None):
        """TransformationMatrix class initialization

        :param matrix: matrix used to initialize the transformationMatrix - MMatrix or matrixList[16]
        :type matrix: list[int, float] or :class:`maya.api.OpenMaya.MMatrix`

        :param translate: value of translation of the transformationMatrix - ONLY IF MATRIX NOT SPECIFIED
        :type translate: list[int, float]

        :param rotate: value of rotation of the transformationMatrix - ONLY IF MATRIX NOT SPECIFIED
        :type rotate: list[int, float]

        :param scale: value of scale of the transformationMatrix - ONLY IF MATRIX NOT SPECIFIED
        :type scale: list[int, float]

        :param shear: value of shear of the transformationMatrix - ONLY IF MATRIX NOT SPECIFIED
        :type shear: list[int, float]

        :param rotateOrder: value of rotateOrder of the transformationMatrix
        :type rotateOrder: :str

        :return: the transformation matrix
        :rtype: :class:`cgp_maya_utils.api.TransformationMatrix`
        """

        pass

    def __repr__(self):
        """the representation of the TransformationMatrix

        :return: the representation of the TransformationMatrix
        :rtype: str
        """

        pass

    # OPERATORS #

    def __mul__(self, matrix):
        """multiply the transformationMatrix by a MtransformationMatrix/MMatrix

        :param matrix: matrix to multiply to the transformationMatrix
        :type matrix: :class:`maya.api.OpenMaya.MMatrix` or :class:`maya.api.OpenMaya.MTransformationMatrix`

        :return: the multiplied transformationMatrix
        :rtype: :class:`cgp_maya_utils.api.TransformationMatrix`
        """

        pass

    # OBJECT COMMANDS #

    @classmethod
    def fromAttribute(cls, attribute, rotateOrder=None):
        """get the transformationMatrix from the specified attribute

        :param attribute: attribute to get the matrix from - transform.matrix
        :type attribute: str or :class:`cgp_maya_utils.scene.Attribute`

        :param rotateOrder: rotateOrder of the transformationMatrix to get - use transform one if nothing specified
        :type rotateOrder: str

        :return: the transformationMatrix
        :rtype: :class:`cgp_maya_utils.api.TransformationMatrix`
        """

        pass

    @classmethod
    def fromMatrix(cls, matrix, rotateOrder=None):
        """get the transformationMatrix from the specified matrix

        :param matrix: MMatrix or 16 numbers list used to initialize the transformationMatrix
        :type matrix: list[int, float] or :class:`maya.api.OpenMaya.MMatrix`

        :param rotateOrder: rotateOrder of the transformationMatrix to get
        :type rotateOrder: str

        :return: the transformationMatrix
        :rtype: :class:`cgp_maya_utils.api.TransformationMatrix`
        """

        pass

    @classmethod
    def fromTransforms(cls, translate=None, rotate=None, scale=None, shear=None, rotateOrder=None):
        """get the transformationMatrix from the specified transforms

        :param translate: value of translation of the transformationMatrix
        :type translate: list[int, float]

        :param rotate: value of rotation of the transformationMatrix
        :type rotate: list[int, float]

        :param scale: value of scale of the transformationMatrix
        :type scale: list[int, float]

        :param shear: value of shear of the transformationMatrix
        :type shear: list[int, float]

        :param rotateOrder: rotateOrder of the transformationMatrix to get
        :type rotateOrder: str

        :return: the transformationMatrix
        :rtype: :class:`cgp_maya_utils.api.TransformationMatrix`
        """

        pass

    # COMMANDS #

    def rotateOrder(self):
        """the rotateOrder of the transformationMatrix

        :return: the rotateOrder
        :rtype: str
        """

        pass

    def setRotateOrder(self, rotateOrder):
        """set the rotateOrder of the transformationMatrix

        :param rotateOrder: new rotateOrder of the transformationMatrix
        :type rotateOrder: str
        """

        pass

    def transformValues(self):
        """the transform values stored in the transformation matrix

        :return: the transform values of the transformation matrix
        :rtype: dict
        """

        pass
