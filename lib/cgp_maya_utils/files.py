"""
maya file objects and management functions
"""

# imports third-parties
import cgp_generic_utils.files
import cgp_generic_utils.constants

# imports local
import cgp_maya_utils.decorators


class MayaFile(cgp_generic_utils.files.File):
    """file object that manipulates any kind of maya file on the file system
    """

    # ATTRIBUTES #

    _dataType = NotImplemented

    # OBJECT COMMANDS #

    @classmethod
    def create(cls, path, content=None, **__):
        """create a maya file

        :param path: path of the maya file
        :type path: str

        :param content: content of the maya file
        :type content: any

        :return: the created maya file
        :rtype: :class:`cgp_maya_utils.files.MayaFile`
        """

        pass

    # COMMANDS #

    def import_(self, asReference=False, namespace=None):
        """import the maya file in the scene

        :param asReference: ``True`` : maya file is referenced - ``False`` maya file is imported without reference
        :type asReference: bool

        :param namespace: namespace of the file to import
        :type namespace: str or :class:`cgp_maya_utils.scene.Namespace`
        """

        pass

    def open(self, force=False):
        """open the maya file

        :param force: ``True`` : opening will be forced - ``False`` : prompt will show before opening
        :type force: bool
        """

        pass


class MaFile(MayaFile):
    """file object that manipulates a ``.ma`` file on the file system
    """

    # ATTRIBUTES #

    _extension = 'ma'
    _dataType = 'mayaAscii'


class MbFile(MayaFile):
    """file object that manipulates a ``.mb`` file on the file system
    """

    # ATTRIBUTES #

    _extension = 'mb'
    _dataType = 'mayaBinary'


class ObjFile(cgp_generic_utils.files.File):
    """file object that manipulates a ``.obj`` file on the file system
    """

    # ATTRIBUTES #

    _extension = 'obj'

    # OBJECT COMMANDS #

    @classmethod
    @cgp_maya_utils.decorators.KeepCurrentSelection()
    def create(cls, path, content=None, **__):
        """create an obj file

        :param path: path of the obj file
        :type path: str

        :param content: nodes from the scene to add to the content of the file - use selection if nothing is specified
        :type content: list[str, :class:`cgp_maya_utils.scene.Node`]

        :return: the created obj file
        :rtype: :class:`cgp_maya_utils.files.ObjFile`
        """

        pass

    # COMMANDS #

    def import_(self, name):
        """imports the obj file

        :param name: name of the imported object
        :type name: str

        :return: the imported object
        :rtype: str
        """

        pass


def registerFileTypes():
    """register maya file types to grant generic file management functions access to the maya file objects
    """

    pass
