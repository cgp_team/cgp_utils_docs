"""
python objects and management functions to manipulate a variety of entities in a maya scene
such as nodes, attributes, components, namespaces, plugins ...
"""

# imports third-parties
import maya.api.OpenMaya

# imports local
import cgp_maya_utils.constants
import cgp_maya_utils.decorators


class Connection(object):
    """connection object that manipulates live or virtual connection between two attributes
    """

    # INIT #

    def __init__(self, source, destination):
        """Connection class initialization

        :param source: attribute that drives the connection - ``node.attribute`` or ``Attribute``
        :type source: str or :class:`cgp_maya_utils.scene.Attribute`

        :param destination: attribute that is driven by the connection - ``node.attribute`` or ``Attribute``
        :type destination: str or :class:`cgp_maya_utils.scene.Attribute`
        """

        pass

    def __repr__(self):
        """the representation of the connection

        :return: the representation of the connection
        :rtype: str
        """

        pass

    # OBJECT COMMANDS #

    @classmethod
    def create(cls, source, destination):
        """create a connection

        :param source: attribute that drives the connection - ``node.attribute``
        :type source: str

        :param destination: attribute that is driven by the connection - ``node.attribute``
        :type destination: str

        :return: the created connection
        :rtype: :class:`cgp_maya_utils.scene.Connection`
        """

        pass

    @classmethod
    def get(cls, node, attributes=None, sources=True, destinations=True, nodeTypes=None,
            nodeTypesIncluded=True, skipConversionNodes=False):
        """get the connections from the specified node

        :param node: node to get the connections from
        :type node: str or :class:`cgp_maya_utils.scene.Node`

        :param attributes: list of attributes to get the connections from - get all if nothing specified
        :type attributes: list[str]

        :param sources: ``True`` : get the source connections - ``False`` does not get source connections
        :type sources: bool

        :param destinations: ``True`` : get the destination connections - ``False`` does not get destination connections
        :type destinations: bool

        :param nodeTypes: types of nodes used to get the connections - All if nothing is specified
        :type nodeTypes: list[str]

        :param nodeTypesIncluded: ``True`` : include specified node types - ``False`` : exclude specified node types
        :type nodeTypesIncluded: bool

        :param skipConversionNodes: ``True`` : conversion nodes are skipped - ``False`` conversion nodes are not skipped
        :type skipConversionNodes: bool

        :return: the connections
        :rtype: list[:class:`cgp_maya_utils.scene.Connection`]
        """

        pass

    # COMMANDS #

    def connect(self):
        """connect the connection
        """

        pass

    def data(self):
        """data necessary to store the compound attribute on disk and/or recreate it from scratch

        :return: the data of the connection
        :rtype: tuple
        """

        pass

    def destination(self):
        """the destination attribute of the connection

        :return: the destination attribute
        :rtype: :class:`cgp_maya_utils.scene.Attribute`
        """

        pass

    def disconnect(self):
        """disconnect the connection
        """

        pass

    def isConnected(self):
        """check if the connection exists or not

        :return: ``True`` : connection is live - ``False`` : connection is virtual
        :rtype: bool
        """

        pass

    def setDestination(self, node=None, attribute=None):
        """set the destination of the connection

        :param node: the new name of the destination node - keep current if None is specified
        :type node: str

        :param attribute: the new name of the destination attribute - keep current if None is specified
        :type attribute: str
        """

        pass

    def setSource(self, node=None, attribute=None):
        """set the source of the connection

        :param node: the new name of the source node - keep current if None is specified
        :type node: str

        :param attribute: the new name of the source attribute - keep current if None is specified
        :type attribute: str
        """

        pass

    def source(self):
        """the source attribute of the connection

        :return: the source attribute
        :rtype: :class:`cgp_maya_utis.scene.Attribute`
        """

        pass


class Attribute(object):
    """attribute object that manipulates any kind of attribute
    """

    # ATTRIBUTES #

    _attributeType = 'attribute'

    # INIT #

    def __init__(self, fullName):
        """Attribute class initialization

        :param fullName: full name of the attribute - ``node.attribute``
        :type fullName: str
        """

        pass

    def __eq__(self, attribute):
        """check if the Attribute is identical to the other attribute

        :param attribute: attribute to compare to
        :type attribute: str or :class:`cgp_maya_utils.scene.Attribute`

        :return: ``True`` : attributes are identical - ``False`` : attributes are different
        :rtype: bool
        """

        pass

    def __ne__(self, attribute):
        """check if the Attribute is different to the other attribute

        :param attribute: attribute to compare to
        :type attribute: str or :class:`cgp_maya_utils.scene.Attribute`

        :return: ``True`` : attributes are different - ``False`` : attributes are identical
        :rtype: bool
        """

        pass

    def __repr__(self):
        """the representation of the attribute

        :return: the representation of the attribute
        :rtype: str
        """

        pass

    def __str__(self):
        """the print of the attribute

        :return: the print the attribute
        :rtype: str
        """

        pass

    # OBJECT COMMANDS #

    @classmethod
    def create(cls, node, name, value=None, connectionSource=None, connectionDestinations=None, **__):
        """create an attribute

        :param node: node on which to create the attribute
        :type node: str

        :param name: name of the attribute to create
        :type name: str

        :param value: value to set on the attribute
        :type value: int or bool

        :param connectionSource: attribute to connect as source - ``node.attribute``
        :type connectionSource: str or :class:`cgp_maya_utils.scene.Attribute`

        :param connectionDestinations: attributes to connect as destination - ``[node1.attribute1, node2.attribute2 ...]``
        :type connectionDestinations: list[str] or list[:class:`cgp_maya_utils.scene.Attribute`]

        :return: the created attribute
        :rtype: :class:`cgp_maya_utils.scene.Attribute`
        """

        pass

    # COMMANDS #

    def attributeType(self):
        """the type of the attribute

        :return: the type of the attribute
        :rtype: str
        """

        pass

    def connect(self, source=None, destinations=None):
        """connect the attribute to the source and destinations

        :param source: source attribute to connect to the attribute - ``node.attribute`` or ``Attribute``
        :type source: str or :class:`cgp_maya_utils.scene.Attribute`

        :param destinations: destination attributes to connect to the attribute - ``[node1.attribute1 ...]`` or ``[Attribute1 ...]``
        :type destinations: list[str] or list[:class:`cgp_maya_utils.scene.Attribute`]
        """

        pass

    def connections(self, nodeTypes=None, source=True, destinations=True, nodeTypesIncluded=True,
                    skipConversionNodes=False):
        """get the connections of the attribute

        :param nodeTypes: types of nodes used to get the connections - All if nothing is specified
        :type nodeTypes: list[str]

        :param source: define whether or not the command will get the source connection
        :type source: bool

        :param destinations: define whether or not the command will get the destination connections
        :type destinations: bool

        :param nodeTypesIncluded: ``True`` : include specified node types - ``False`` : exclude specified node types
        :type nodeTypesIncluded: bool

        :param skipConversionNodes: ``True`` : conversion nodes are skipped - ``False`` conversion nodes are not skipped
        :type skipConversionNodes: bool

        :return: the connections of the attribute
        :rtype: list[:class:`cgp_maya_utils.scene.Connection`]
        """

        pass

    def data(self, skipConversionNodes=True):
        """data necessary to store the attribute on disk and/or recreate it from scratch

        :param skipConversionNodes: ``True`` : conversion nodes are skipped - ``False`` conversion nodes are not skipped
        :type skipConversionNodes: bool

        :return: the data of the attribute
        :rtype: dict
        """

        pass

    def delete(self):
        """delete the attribute
        """

        pass

    def disconnect(self, source=True, destinations=True, destinationAttributes=None):
        """disconnect the attribute from the source and the specified destinations

        :param source: defines whether or not the source connected attribute will be disconnected
        :type source: bool

        :param destinations: defines whether or not the destination connected attribute will be disconnected
        :type destinations: bool

        :param destinationAttributes: destination attributes to disconnect from the attribute - All if None is specified
        :type destinationAttributes: list
        """

        pass

    def fullName(self):
        """get the full name of the attribute

        :return: the full name of the attribute - ``node.attribute``
        :rtype: str
        """

        pass

    def isLocked(self):
        """check if the attribute is locked

        :return: ``True`` : it is locked - ``False`` : it is not locked
        :rtype: bool
        """

        pass

    def isUserDefined(self):
        """check if the attribute is user defined

        :return: ``True`` : it is user defined - ``False`` : it is not user defined
        :rtype: bool
        """

        pass

    def name(self):
        """the name of the attribute

        :return: the name of the attribute
        :rtype: str
        """

        pass

    def node(self):
        """the node on which the attribute lives

        :return: the node of the attribute
        :rtype: :class:`cgp_maya_utils.scene.Node`
        """

        pass

    def setLock(self, isLocked):
        """set the lock state of the attribute

        :param isLocked: ``True`` attribute will be locked - ``False`` : attribute will be unlocked
        :type isLocked: bool
        """

        pass

    def setName(self, name):
        """set the name of the attribute

        :param name: new name of the attribute
        :type name: str
        """

        pass

    def setValue(self, value):
        """set the value on the attribute

        :param value: value used to set the attribute
        :type value: any
        """

        pass

    def value(self):
        """the value of the attribute

        :return: the value of the attribute
        :rtype: any
        """

        pass


class CompoundAttribute(Attribute):
    """attribute object that manipulates any kind of compound attribute
    """

    # ATTRIBUTES #

    _attributeType = cgp_maya_utils.constants.AttributeType.COMPOUND
    _attributeSubType = NotImplemented

    # OBJECT COMMANDS #

    @classmethod
    def create(cls, node, name, components=None, value=None, connections=None, **__):
        """create a compound attribute

        :param node: node on which to create the attribute
        :type node: str

        :param name: name of the attribute to create
        :type name: str

        :param components: components used to create the attribute - default is Axis.ALL
        :type components: any

        :param value: value to set on the attribute
        :type value: list[any]

        :param connections: connection to build on the attribute - {'source': obj.attr, 'destinations': [obj.attr ...]}
        :type connections: dict

        :return: the created attribute
        :rtype: :class:`cgp_maya_utils.scene.CompoundAttribute`
        """

        pass

    # COMMANDS #

    def components(self):
        """the components of the compound attribute

        :return: the components of the compound attribute
        :rtype: list[:class:`cgp_maya_utils.sceneAttribute`]
        """

        pass

    def data(self, skipConversionNodes=True):
        """data necessary to store the compound attribute on disk and/or recreate it from scratch

        :param skipConversionNodes: ``True`` : conversion nodes are skipped - ``False`` conversion nodes are not skipped
        :type skipConversionNodes: bool

        :return: the data of the compound attribute
        :rtype: dict
        """

        pass

    def setValue(self, value):
        """set the value of the compound attribute

        :param value: value to set on the compound attribute
        :type value: list[any]
        """

        pass

    def subType(self):
        """the subType of the compound attribute

        :return: the subType of the compound attribute
        :rtype: str
        """

        pass

    def value(self):
        """the value of the compound attribute

        :return: the value of the compound attribute
        :rtype: any
        """

        pass


class Double3Attribute(CompoundAttribute):
    """attribute object that manipulates ``double3`` attribute
    """

    # ATTRIBUTES #

    _attributeType = cgp_maya_utils.constants.AttributeType.DOUBLE3
    _attributeSubType = cgp_maya_utils.constants.AttributeType.DOUBLE


class Float3Attribute(CompoundAttribute):
    """attribute object that manipulates ``float3`` attribute
    """

    # ATTRIBUTES #

    _attributeType = cgp_maya_utils.constants.AttributeType.FLOAT3
    _attributeSubType = cgp_maya_utils.constants.AttributeType.FLOAT


class TDataCompoundAttribute(CompoundAttribute):
    """attribute object that manipulates ``TDataCompound`` attribute
    """

    # ATTRIBUTES #

    _attributeType = cgp_maya_utils.constants.AttributeType.TDATACOMPOUND
    _attributeSubType = None

    # OBJECT COMMANDS #

    @classmethod
    def create(cls, *_, **__):
        """create a TDataCompound attribute
        """

        pass

    # COMMANDS #

    def setValue(self, value):
        """set the value of the TDataCompound attribute

        :param value: value to set to the TDataCompound attribute
        :type value: dict
        """

        pass

    def value(self):
        """the value of the TDataCompound attribute

        :return: the value of the TDataCompound attribute
        :rtype: dict
        """

        pass


class BoolAttribute(Attribute):
    """attribute object that manipulates a ``boolean`` attribute
    """

    # ATTRIBUTES #

    _attributeType = cgp_maya_utils.constants.AttributeType.BOOLEAN

    # COMMANDS #

    def setValue(self, value):
        """set the value on the attribute

        :param value: value to set on the boolean attribute
        :type value: bool or int
        """

        pass


class EnumAttribute(Attribute):
    """attribute object that manipulates a ``enum`` attribute
    """

    # ATTRIBUTES #

    _attributeType = cgp_maya_utils.constants.AttributeType.ENUM

    # OBJECT COMMANDS #

    @classmethod
    def create(cls, node, name, items=None, value=None, connections=None, **__):
        """create an enum attribute

        :param node: node on which to create the attribute
        :type node: str

        :param name: name of the attribute to create
        :type name: str

        :param items: items used to create the attribute
        :type items: list[str]

        :param value: value to set on the attribute
        :type value: int or str

        :param connections: connection to build on the attribute - ``{'source': node.attribute, 'destinations': [node.attribute ...]}``
        :type connections: dict

        :return: the created attribute
        :rtype: :class:`cgp_maya_utils.scene.EnumAttribute`
        """

        pass

    # COMMANDS #

    def data(self, skipConversionNodes=True):
        """data necessary to store the enum attribute on disk and/or recreate it from scratch

        :param skipConversionNodes: ``True`` : conversion nodes are skipped - ``False`` conversion nodes are not skipped
        :type skipConversionNodes: bool

        :return: the data of the enum attribute
        :rtype: dict
        """

        pass

    def items(self):
        """get the items of the enum attribute

        :return: the items of the enum attribute
        :rtype: list[str]
        """

        pass

    def setValue(self, value):
        """set the value on the enum attribute

        :param value: value to set on the enum attribute
        :type value: float or str
        """

        pass

    def value(self, asString=True):
        """the value of the enum attribute

        :param asString: ``True`` : value is returned as a string - ``False`` : value is returned as an integer
        :type asString: bool

        :return: the value of the enum attribute
        :rtype: int or str
        """

        pass


class MatrixAttribute(Attribute):
    """attribute object that manipulates a ``matrix`` attribute
    """

    # ATTRIBUTES #

    _attributeType = cgp_maya_utils.constants.AttributeType.MATRIX

    # COMMANDS #

    def setValue(self, value):
        """set the value on the matrix attribute

        :param value: value to set on the matrix attribute
        :type value: list[int, float]
        """

        pass

    def transformationMatrix(self, rotateOrder=None):
        """the transformationMatrix related to the MMatrix stored in the matrix attribute

        :param rotateOrder: rotateOrder of the transformationMatrix to get - use transform one if nothing specified
        :type rotateOrder: str

        :return: the transformationMatrix
        :rtype: :class:`cgp_maya_utils.api.TransformationMatrix`
        """

        pass


class MessageAttribute(Attribute):
    """attribute object that manipulates a ``message`` attribute
    """

    # ATTRIBUTES #

    _attributeType = cgp_maya_utils.constants.AttributeType.MESSAGE

    # COMMANDS #

    def setValue(self, value):
        """set the value on the message attribute

        :param value: value to set on the message attribute
        :type value: str or :class:`cgp_maya_utils.scene.Attribute`
        """

        pass

    def value(self):
        """the value of the message attribute

        :return: the value of the message attribute
        :rtype: str
        """

        pass


class StringAttribute(Attribute):
    """attribute object that manipulates a ``string`` attribute
    """

    # ATTRIBUTES #

    _attributeType = cgp_maya_utils.constants.AttributeType.STRING

    # COMMANDS #

    def eval(self):
        """eval the content of the string attribute

        :return: the eval content
        :rtype: literal
        """

        pass

    def setValue(self, value):
        """set the value on the attribute

        :param value: value used to set the attribute
        :type value: str
        """

        pass


class NumericAttribute(Attribute):
    """attribute object that manipulates any kind of numeric attribute
    """

    # ATTRIBUTES #

    _attributeType = 'numeric'

    # INIT #

    def __le__(self, value):
        """override le operator

        :param value: value to compare to the numeric attribute
        :type value: numeric

        :return: ``True`` : attribute is less than or equal to the value - ``False`` attribute is greater than the value
        :rtype: bool
        """

        pass

    def __lt__(self, value):
        """override lt operator

        :param value:value to compare to the numeric attribute
        :type value: numeric

        :return: ``True`` : attribute is less than the value - ``False`` attribute is greater than or equal to the value
        :rtype: bool
        """

        pass

    def __ge__(self, value):
        """override ge operator

        :param value : value to compare to the numeric attribute
        :type value: int or float

        :return: ``True`` : attribute is greater than or equal to the value - ``False`` attribute is less than the value
        :rtype: bool
        """

        pass

    def __gt__(self, value):
        """override gt operator

        :param value: value to compare to the numeric attribute
        :type value: int or float

        :return: ``True`` : attribute is greater than the value - ``False`` attribute is less than or equal to the value
        :rtype: bool
        """

        pass

    # COMMANDS #

    def add(self, value):
        """add the value to the numeric attribute

        :param value: value to add to the numeric attribute
        :type value: int or float
        """

        pass

    def setValue(self, value):
        """set the value on the numeric attribute

        :param value: value to set on the numeric attribute
        :type value: int or float
        """

        pass

    def substract(self, value):
        """substract the value to the numeric attribute

        :param value: value to substract to the numeric
        :type value: int or float
        """

        pass

    def multiply(self, value):
        """multiply the numeric attribute by the value

        :param value: value to multiply to the numeric attribute
        :type value: int or float
        """

        pass

    def divide(self, value):
        """divide the numeric attribute by the value

        :param value: value to divide to the numeric attribute
        :type value: int or float
        """

        pass

    def power(self, value):
        """power the numeric attribute by the value

        :param value: value to power the numeric attribute by
        :type value: int or float
        """

        pass


class ByteAttribute(NumericAttribute):
    """attribute object that manipulates a ``byte`` attribute
    """

    # ATTRIBUTES #

    _attributeType = cgp_maya_utils.constants.AttributeType.BYTE


class DoubleAttribute(NumericAttribute):
    """attribute object that manipulates a ``double`` attribute
    """

    # ATTRIBUTES #

    _attributeType = cgp_maya_utils.constants.AttributeType.DOUBLE


class DoubleAngleAttribute(NumericAttribute):
    """attribute object that manipulates a ``doubleAngle`` attribute
    """

    # ATTRIBUTES #

    _attributeType = cgp_maya_utils.constants.AttributeType.DOUBLE_ANGLE


class DoubleLinearAttribute(NumericAttribute):
    """attribute object that manipulates a ``doubleLinear`` attribute
    """

    # ATTRIBUTES #

    _attributeType = cgp_maya_utils.constants.AttributeType.DOUBLE_LINEAR


class FloatAttribute(NumericAttribute):
    """attribute object that manipulates a ``float`` attribute
    """

    # ATTRIBUTES #

    _attributeType = cgp_maya_utils.constants.AttributeType.FLOAT


class LongAttribute(NumericAttribute):
    """attribute object that manipulates a ``long`` attribute
    """

    # ATTRIBUTES #

    _attributeType = cgp_maya_utils.constants.AttributeType.LONG


class ShortAttribute(NumericAttribute):
    """attribute object that manipulates a ``short`` attribute
    """

    # ATTRIBUTES #

    _attributeType = cgp_maya_utils.constants.AttributeType.SHORT


class TimeAttribute(NumericAttribute):
    """attribute object that manipulates a ``time`` attribute
    """

    # ATTRIBUTES #

    _attributeType = cgp_maya_utils.constants.AttributeType.TIME


class Component(object):
    """component object that manipulates any kind of component
    """

    # ATTRIBUTES #

    _componentType = 'component'

    # INIT #

    def __init__(self, fullName):
        """Component class initialization

        :param fullName: full name of the component - ``shape.component[*]`` or ``shape.component[*][*]``
        :type fullName: str
        """

        pass

    def __eq__(self, component):
        """check if the Component is identical to the other component

        :param component: component to compare to
        :type component: str or :class:`cgp_maya_utils.scene.Component`

        :return: ``True`` : components are identical - ``False`` : components are different
        :rtype: bool
        """

        pass

    def __ne__(self, component):
        """check if the Component is different to the other component

        :param component: component to compare to
        :type component: str or :class:`cgp_maya_utils.scene.Component`

        :return: ``True`` : components are different - ``False`` : components identical
        :rtype: bool
        """

        pass

    def __repr__(self):
        """the representation of the component

        :return: the representation of the component
        :rtype: str
        """

        pass

    def __str__(self):
        """the print of the component

        :return: the print of the component
        :rtype: str
        """

        pass

    # COMMANDS #

    def componentType(self):
        """the type of the component

        :return: the type of the component
        :rtype: str
        """

        pass

    def indexes(self):
        """the indexes of the component

        :return: the indexes of the component
        :rtype: list[int]
        """

        pass

    def fullName(self):
        """the full name of the component

        :return: the full name of the component - ``node.component[*]`` or ``node.component[*][*]``
        :rtype: str
        """

        pass

    def name(self):
        """the name of the component

        :return: the name of the component - ``component[*]`` or ``component[*][*]``
        :rtype: str
        """

        pass

    def shape(self):
        """the shape of the component

        :return: the shape of the component
        :rtype: :class:`cgp_maya_utils.scene.Shape`
        """

        pass


class TransformComponent(Component):
    """component object that manipulates any kind of transform component
    """

    # ATTRIBUTES #

    _componentType = 'transformComponent'

    # INIT #

    def __init__(self, fullName):
        """TransformComponent class initialization

        :param fullName: full name of the transform component - ``shape.component[*]`` or ``shape.component[*][*]``
        :type fullName: str
        """

        pass

    # COMMANDS #

    def position(self, worldSpace=False):
        """the position of the transform component

        :param worldSpace: ``True`` : positions will be worldSpace - ``False`` : positions will be local
        :type worldSpace: bool

        :return: the position of the component
        :rtype: list[float]
        """

        pass


class Edge(TransformComponent):
    """component object that manipulates an ``edge`` component
    """

    # ATTRIBUTES #

    _componentType = cgp_maya_utils.constants.ComponentType.EDGE


class Face(TransformComponent):
    """component object that manipulates an ``face`` component
    """

    # ATTRIBUTES #

    _componentType = cgp_maya_utils.constants.ComponentType.FACE


class Vertex(TransformComponent):
    """component object that manipulates an ``vertex`` component
    """

    # ATTRIBUTES #

    _componentType = cgp_maya_utils.constants.ComponentType.VERTEX


class CurvePoint(TransformComponent):
    """component object that manipulates a ``curvePoint`` component
    """

    # ATTRIBUTES #

    _componentType = cgp_maya_utils.constants.ComponentType.CURVE_POINT


class EditPoint(TransformComponent):
    """component object that manipulates an ``editPoint`` component
    """

    # ATTRIBUTES #

    _componentType = cgp_maya_utils.constants.ComponentType.EDIT_POINT


class IsoparmU(Component):
    """component object that manipulates an ``isoparmU`` component
    """

    # ATTRIBUTES #

    _componentType = cgp_maya_utils.constants.ComponentType.ISOPARM_U


class IsoparmV(Component):
    """component object that manipulates an ``isoparmV`` component
    """

    # ATTRIBUTES #

    _componentType = cgp_maya_utils.constants.ComponentType.ISOPARM_V


class SurfacePatch(Component):
    """component object that manipulates an ``surfacePatch`` component
    """

    # ATTRIBUTES #

    _componentType = cgp_maya_utils.constants.ComponentType.SURFACE_PATCH


class SurfacePoint(TransformComponent):
    """component object that manipulates an ``surfacePoint`` component
    """

    # ATTRIBUTES #

    _componentType = cgp_maya_utils.constants.ComponentType.SURFACE_POINT


class Namespace(object):
    """namespace object that manipulates a namespace
    """

    # INIT #

    def __init__(self, namespace):
        """Namespace class initialization

        :param namespace: name of the namespace - if namespace is not absolute, it will search in the current namespace
        :type namespace: str
        """

        pass

    def __eq__(self, namespace):
        """check if the Namespace has the same name as the other namespace

        :param namespace: namespace to check the name with
        :type namespace: Namespace / str

        :return: True if the names are identical, False otherwise
        :rtype: bool
        """

        pass

    def __ne__(self, namespace):
        """check if the Namespace has not the same name as the other namespace

        :param namespace: namespace to check the name with
        :type namespace: Namespace / str

        :return: True if the names are different, False otherwise
        :rtype: bool
        """

        pass

    def __repr__(self):
        """get the representation of the namespace

        :return: the representation of the namespace
        :rtype: str
        """

        pass

    def __str__(self):
        """get the print of the namespace

        :return: the print of the namespace
        :rtype: str
        """

        pass

    # OBJECT COMMANDS #

    @classmethod
    def create(cls, name):
        """create a namespace

        :param name: name of the namespace to create
        :type name: str

        :return: the created namespace
        :rtype: :class:`cgp_maya_utils.scene.Namespace`
        """

        pass

    # COMMANDS #

    def fullName(self, asAbsolute=True):
        """the full name of the namespace

        :param asAbsolute: ``True`` : full name is returned asAbsolute - ``False`` full name is returned as relative
        :type asAbsolute: bool

        :return: the full name of the namespace
        :rtype: str
        """

        pass

    def isCurrent(self):
        """check is the namespace is the current namespace
        """

        pass

    def name(self):
        """the name of the namespace

        :return: the name of the namespace
        :rtype: str
        """

        pass

    def parent(self):
        """the parent namespace of the namespace
        """

        pass

    def setAsCurrent(self):
        """set the namespace as current namespace
        """

        pass


class Plugin(object):
    """plugin object that manipulates a maya plugin
    """

    # INIT #

    def __init__(self, name):
        """Plugin class initialization

        :param name: name of the plugin
        :type name: str
        """

        pass

    def __repr__(self):
        """override plugin representation

        :return: the representation of the plugin
        :rtype: str
        """

        pass

    # COMMANDS #

    def isAutoLoaded(self):
        """check if the plugin is autoLoaded

        :return: ``True`` : it is autoLoaded  - ``False`` : it is not autoLoaded
        :rtype: bool
        """

        pass

    def isLoaded(self):
        """check if the plugin is loaded

        :return: ``True`` : it is loaded  - ``False`` : it is not loaded
        :rtype: bool
        """

        pass

    def load(self):
        """load the plugin if it is not already loaded
        """

        pass

    def name(self):
        """the name of the plugin

        :return: the name of the plugin
        :rtype: str
        """

        pass

    def path(self):
        """the path of the plugin file

        :return: the path of the plugin file
        :rtype: :class:`cgp_generic_utils.files.File`
        """

        pass

    def unload(self, deleteNodes=False):
        """unload the plugin

        :param deleteNodes: ``True`` : plugin nodes are deleted before unload - ``False`` nodes are not deleted
        :type deleteNodes: bool
        """

        pass


class Scene(object):
    """scene object that manipulates a live scene
    """

    # INIT #

    def __repr__(self):
        """the representation of the scene object

        :return: the representation of the scene object
        :rtype: str
        """

        pass

    # STATIC COMMANDS #

    @staticmethod
    def animationStart():
        """the start of the animation

        :return: the start of the animation
        :rtype: float
        """

        pass

    @staticmethod
    def animationEnd():
        """the end of the animation

        :return: the end of the animation
        :rtype: float
        """

        pass

    @staticmethod
    def currentTime():
        """the current time of the animation

        :return: the current time of the animation
        :rtype: float
        """

        pass

    @staticmethod
    def file_():
        """get the file of the scene

        :return: the file of the scene
        :rtype: :class:`cgp_generic_utils.files.File`
        """

        pass

    @staticmethod
    def mainWindow():
        """get the maya main window

        :return: the main window
        :rtype: :class:`PySide2.QtWidgets.QMainWindow`
        """
        pass

    @staticmethod
    def maximumTime():
        """the maximum time of the animation

        :return: the maximum time of the animation
        :rtype: float
        """

        pass

    @staticmethod
    def minimumTime():
        """the minimum time of the animation

        :return: the minimum time of the animation
        :rtype: float
        """

        pass

    @staticmethod
    def new(force=False):
        """open a new scene

        :param force: ``True`` : new scene opening will be forced - ``False`` : save prompt will show before opening
        :type force: bool
        """

        pass

    @staticmethod
    def setAnimationStart(value):
        """set the start of the animation

        :param value: value used to set the animationStart
        :type value: float or int
        """

        pass

    @staticmethod
    def setAnimationEnd(value):
        """set the end of the animation

        :param value: value used to set the animationEnd
        :type value: float or int
        """

        pass

    @staticmethod
    def setCurrentTime(value):
        """set the currentTime of the animation

        :param value: value used to set the currentTime
        :type value: float or int
        """

        pass

    @staticmethod
    def setMaximumTime(value):
        """set the maximumTime of the animation

        :param value: value used to set the maximumTime
        :type value: float or int
        """

        pass

    @staticmethod
    def setMinimumTime(value):
        """set the minimumTime of the animation

        :param value: value used to set the minimumTime
        :type value: float or int
        """

        pass

    @staticmethod
    def viewport():
        """the viewport of the scene

        :return: the viewport of the scene
        :rtype: str
        """

        pass

    # COMMANDS #

    def reopen(self, force=False):
        """reopen the scene file

        :param force: ``True`` : scene reopening will be forced - ``False`` : save prompt will show before reopening
        :type force: bool
        """

        pass


class Node(object):
    """node object that manipulates any kind of node
    """

    # ATTRIBUTES #

    _nodeType = 'baseNode'

    # INIT #

    def __init__(self, name):
        """Node class initialization

        :param name: name of the node
        :type name: str
        """

        pass

    def __eq__(self, node):
        """check if the Node is identical to the other node

        :param node: node to compare to
        :type node: str or :class:`cgp_maya_utils.scene.Node`

        :return: ``True`` : nodes are identical - ``False`` : nodes are different
        :rtype: bool
        """

        pass

    def __ne__(self, node):
        """check if the Node is different to the other node

        :param node: node to compare to
        :type node: str or :class:`cgp_maya_utils.scene.Node`

        :return: ``True`` : nodes are different - ``False`` : nodes are identical
        :rtype: bool
        """

        pass

    def __repr__(self):
        """the representation of the node

        :return: the representation of the node
        :rtype: str
        """

        pass

    def __str__(self):
        """the print of the node

        :return: the print of the node
        :rtype: str
        """

        pass

    # OBJECT COMMANDS #

    @classmethod
    def create(cls, nodeType, connections=None, attributeValues=None, name=None, **__):
        """create a node

        :param nodeType: type of node to create
        :type nodeType: str

        :param connections: connections to set on the created node
        :type connections: list[tuple[str]]

        :param attributeValues: attribute values to set on the node
        :type attributeValues: dict

        :param name: name of the node
        :type name: str

        :return: the created node
        :rtype: :class:`cgp_maya_utils.scene.Node`
        """

        pass

    # COMMANDS #

    def attribute(self, attribute):
        """the attribute of the node from an attribute name

        :param attribute: name of the attribute to get
        :type attribute: str

        :return: the attribute object
        :rtype: :class:`cgp_maya_utils.scene.Attribute`
        """

        pass

    def attributes(self, attributeTypes=None, attributeTypesIncluded=True, onlyUserDefined=False):
        """the attributes of the node from attribute types

        :param attributeTypes: types of attributes to get - All if nothing is specified
        :type attributeTypes: list[str]

        :param attributeTypesIncluded: ``True`` : attribute types are included - ``False`` : attribute types are excluded
        :type attributeTypesIncluded: bool

        :param onlyUserDefined: ``True`` : only user defined attributes - ``False`` : not only user defined attributes
        :type onlyUserDefined: bool

        :return: the attributes
        :rtype: list[:class:`cgp_maya_utils.scene.Attribute`]
        """

        pass

    def attributeValues(self):
        """the values of the available attributes of the node

        :return: the attribute values
        :rtype: dict
        """

        pass

    def connections(self, attributes=None, sources=True, destinations=True, nodeTypes=None, nodeTypesIncluded=True,
                    skipConversionNodes=False):
        """the source and destination connections of the node

        :param attributes: attributes of the node to get the connections from - get all if nothing specified
        :type attributes: list[str]

        :param sources: ``True`` : get the source connections - ``False`` : does not get the source connections
        :type sources: bool

        :param destinations: ``True`` : get the destination connections - ``False`` : does not get the destination connections
        :type destinations: bool

        :param nodeTypes: types of nodes to get the connections from - All if nothing is specified
        :type nodeTypes: list[str]

        :param nodeTypesIncluded: ``True`` : include specified node types - ``False`` : exclude specified node types
        :type nodeTypesIncluded: bool

        :param skipConversionNodes: ``True`` : conversion nodes are skipped - ``False`` conversion nodes are not skipped
        :type skipConversionNodes: bool

        :return: the connections of the node
        :rtype: list[:class:`cgp_maya_utils.scene.Connection`]
        """

        pass

    def data(self):
        """data necessary to store the node on disk and/or recreate it from scratch

        :return: the data of the node
        :rtype: dict
        """

        pass

    def delete(self):
        """delete the node
        """

        pass

    def duplicate(self):
        """duplicate the node

        :return: the duplicated node
        :rtype: :class:`cgp_maya_utils.scene.Node`
        """

        pass

    def hasAttribute(self, attribute):
        """check if the node has the attribute

        :param attribute: the attribute to check
        :type attribute: str

        :return: ``True`` : it has the attribute - ``False`` : it does not have the attribute
        :rtype: bool
        """

        pass

    def hasAttributes(self, attributes):
        """check if the node has the attributes

        :param attributes: attributes to check on the node
        :type attributes: list[str]

        :return: ``True`` : it has the attributes - ``False`` : it does not have the attributes
        :rtype: bool
        """

        pass

    def isLocked(self):
        """check if the node is locked

        :return: ``True`` : it is locked - ``False`` : it is not locked
        :rtype: bool
        """

        pass

    def isReferenced(self):
        """check if the node is referenced

        :return: ``True`` : it is referenced - ``False`` : it is not referenced
        :rtype: bool
        """

        pass

    def MFn(self):
        """the function set of the node

        :return: the function set of the node
        :rtype: :class:`maya.api.OpenMaya.MFn`
        """

        pass

    def MObject(self):
        """the MObject of the node

        :return: the MObject of the node
        :rtype: :class:`maya.api.OpenMaya.MObject`
        """

        pass

    def name(self):
        """the name of the node

        :return: the name of the node
        :rtype: str
        """

        pass

    def namespace(self):
        """the namespace of the node

        :return: the namespace of the node
        :rtype: :class:`cgp_maya_utils.scene.Namespace`
        """

        pass

    def nodeType(self):
        """the type of the node

        :return: the type of the node
        :rtype: str
        """

        pass

    def rebuild(self):
        """rebuild the node
        """

        pass

    def reference(self):
        """the reference of the node

        :return: the reference object
        :rtype: :class:`cgp_maya_utils.scene.Reference`
        """

        pass

    def select(self):
        """select the node
        """

        pass

    def setConnections(self, connections):
        """set the connections of the node

        :param connections: connection data used to set the connections
        :type connections: list[tuple[str]]
        """

        pass

    def setLock(self, isLocked):
        """set the lock status of the node

        :param isLocked: ``True`` : the node will be locked - ``False`` : the node will be unlocked
        :type isLocked: bool
        """

        pass

    def setName(self, name):
        """set the name of the node

        :param name: new name of the node
        :type name: str
        """

        pass

    def setAttributeValues(self, attributeValues):
        """set the values of the attributes of the node

        :param attributeValues: values to set to the attributes of the node - ``{attr1: value1, attr2: value2 ...}``
        :type attributeValues: dict
        """

        pass

    # PRIVATE COMMANDS #

    def _availableAttributes(self):
        """the attributes that are listed by the ``Node.attributes`` function

        :return: the available attributes
        :rtype: list[str]
        """

        pass


class DagNode(Node):
    """node object that manipulates any kind of dag node
    """

    # ATTRIBUTES #

    _nodeType = 'dagNode'

    # INIT #

    def __init__(self, name):
        """DagNode class initialization

        :param name: name of the dagNode
        :type name: str
        """

        pass
    # COMMANDS #

    def children(self, namePattern=None, nodeTypes=None, asExactNodeTypes=False, recursive=False):
        """get children nodes of the node

        :param namePattern: pattern the child name has to match - ex: '*pelvis*'
        :type namePattern: str

        :param nodeTypes: node types the child has to match
        :type nodeTypes: list[str]

        :param asExactNodeTypes: ``True`` : list only exact node types - ``False`` : all types inheriting node types
        :type asExactNodeTypes: bool

        :param recursive: ``True`` : get children recursively - ``False`` : get only direct children
        :type recursive: bool

        :return: the children nodes
        :rtype: list[:class:`rdo_maya_rig_utils.scene.DagNode`]
        """

        pass

    def fullName(self):
        """the full name of the node

        :return: the full name of the node
        :rtype: str
        """

        pass

    def name(self):
        """the the shortest unique name of the node

        :return: the name of the node
        :rtype: str
        """

        pass

    def parent(self):
        """the parent of the dag node

        :return: the parent of the dag node
        :rtype: :class:`cgp_maya_utils.scene.DagNode`
        """

        pass

    def setParent(self, parent=None):
        """set the parent of the dag node

        :param parent: dag node to parent the dag node to - If None, parent is the root of the scene
        :type parent: str or :class:`cgp_maya_utils.scene.DagNode`
        """

        pass


class Reference(Node):
    """node object that manipulates a ``reference`` node
    """

    # ATTRIBUTES

    _nodeType = 'reference'

    # INIT #

    def __init__(self, name):
        """Reference class initialization

        :param name: name of the reference node
        :type name: str
        """

        pass

    # OBJECT COMMANDS #

    @classmethod
    def create(cls, path=None, namespace=None, **__):
        """create a reference

        :param path: path of the file to reference
        :type path: str or :class:`cgp_maya_utils.files.MayaFile`

        :param namespace: namespace of the created reference
        :type namespace: str

        :return: the created reference
        :rtype: :class:`cgp_maya_utils.scene.Reference`
        """

        pass

    # COMMANDS #

    def data(self):
        """data necessary to store the reference node on disk and/or recreate it from scratch

        :return: the data of the reference
        :rtype: dict
        """

        pass

    def delete(self):
        """delete the reference
        """

        pass

    def file_(self):
        """the file associated to the reference node

        :return: the file of the reference node
        :rtype: :class:`cgp_maya_utils.scene.MayaFile`
        """

        pass

    def import_(self, namespaceSubstitute=None):
        """import the objects from reference

        :param namespaceSubstitute: string that will replace the ``:`` of the namespace
        :type namespaceSubstitute: str
        """

        pass

    def namespace(self):
        """the namespace of the node

        :return: the namespace of the node
        :rtype: :class:`cgp_maya_utils.scene.Namespace`
        """

        pass

    def setNamespace(self, namespace, renameNode=True):
        """set the namespace

        :param namespace: namespace to set
        :type namespace: str or :class:`cgp_maya_utils.scene.Namespace`

        :param renameNode: ``True`` : the reference node is renamed - ``False`` : the reference node is not renamed
        :type renameNode: bool
        """

        pass


class AnimCurve(Node):
    """node object that manipulates any kind of animation curve node
    """

    # ATTRIBUTES #

    _nodeType = 'animCurve'

    # OBJECT COMMANDS #

    @classmethod
    def create(cls, keys=None, drivenAttributes=None, attributeValues=None, name=None, connections=None, **__):
        """create an animation curve node

        :param keys: keys defining the profile of the animation curve - ``[AnimKey1.data(), AnimKey2.data() ...]``
        :type keys: list[dict]

        :param drivenAttributes: attributes driven by the animation curve node
        :type drivenAttributes: list[str]

        :param attributeValues: attribute values to set on the animation curve node
        :type attributeValues: dict

        :param name: name of the animation curve node
        :type name: str

        :param connections: connections to set on the animation curve node
        :type connections: list[tuple]

        :return: the animation curve object
        :rtype: :class:`cgp_maya_utils.scene.AnimCurve`
        """

        pass

    # COMMANDS #

    def data(self):
        """data necessary to store the animation curve node on disk and/or recreate it from scratch

        :return: the data of the animation curve
        :rtype: dict
        """

        pass

    def drivenAttributes(self):
        """the attributes driven by the anim curve node

        :return: the driven attributes
        :rtype: list[:class:`cgp_maya_utils.scene.Attribute`]
        """

        pass

    def keys(self):
        """the animation keys of the animation curve node

        :return: the animation keys
        :rtype: list[:class:`cgp_maya_utils.api.AnimKey`]
        """

        pass

    def addAnimKey(self, frame=None, value=None, inTangentType=None, outTangentType=None, **__):
        """add an animation key on the animation curve

        :param frame: frame of the animation key - current frame if None is specified
        :type frame: float

        :param value: value of the animation key - key will be inserted if None is specified
        :type value: float

        :param inTangentType: type of the inTangent of the animation key - default is ``cgp_maya_utils.constants.TangentType.AUTO``
        :type inTangentType: str

        :param outTangentType: type of the outTangent of the animation key - - default is ``cgp_maya_utils.constants.TangentType.AUTO``
        :type outTangentType: str

        :return: the added animation key
        :rtype: :class:`cgp_maya_utils.api.AnimKey`
        """

        pass

    # PRIVATE COMMANDS #

    def _availableAttributes(self):
        """the attributes that are listed by the ``Node.attributes`` function

        :return: the available attributes
        :rtype: list[str]
        """

        pass


class AnimCurveTA(AnimCurve):
    """node object that manipulates an ``animCurveTA`` animation curve node
    """

    # ATTRIBUTES #

    _nodeType = 'animCurveTA'


class AnimCurveTL(AnimCurve):
    """node object that manipulates an ``animCurveTL`` animation curve node
    """

    # ATTRIBUTES #

    _nodeType = 'animCurveTL'


class AnimCurveTU(AnimCurve):
    """node object that manipulates an ``animCurveTU`` animation curve node
    """

    # ATTRIBUTES #

    _nodeType = 'animCurveTU'


class Constraint(DagNode):
    """node object that manipulates any kind of constraint node
    """

    # ATTRIBUTES #

    _nodeType = 'constraint'

    # COMMANDS #

    def data(self):
        """data necessary to store the constraint node on disk and/or recreate it from scratch

        :return: the data of the constraint
        :rtype: dict
        """

        pass

    def drivenTransform(self):
        """the transform that is driven by the constraint

        :return: the driven transform
        :rtype: :class:`cgp_maya_utils.scene.Transform` or :class:`cgp_maya_utils.scene.Joint`
        """

        pass

    def drivenAttributes(self):
        """the attributes of the driven transform that are driven by the constraint

        :return: the driven attributes
        :rtype: list[:class:`cgp_maya_utils.scene.Attribute`]
        """

        pass

    def driverTransforms(self):
        """the transforms that drives the constraint

        :return: the driver transforms
        :rtype: list[:class:`cgp_maya_utils.scene.Transform`, :class:`cgp_maya_utils.scene.Joint`]
        """

        pass

    def isValid(self):
        """check is the constraint is valid by verifying if it has driver and driven transforms connected

        :return: ``True`` : the constraint is valid - ``False`` : the constraint is invalid
        :rtype: bool
        """

        pass

    def setWeights(self, values):
        """set the weights of the constraint

        :param values: the values of the weights to set
        :type values: list[int, float]
        """

        pass

    def weights(self):
        """get the weights of the constraint

        :return: the weights of the constraint
        :rtype: list[float]
        """

        pass

    def weightAttributes(self):
        """get the weight attributes

        :return: the weight attributes
        :rtype: list[:class:`cgp_maya_utils.scene.DoubleAttribute`]
        """

        # return
        pass

    # PRIVATE COMMANDS #

    def _driverInputs(self):
        """the input attributes of the constraint that are connected to the drivers of the constraint
        Those attributes ares scanned to get the driver nodes through connection

        :return: the input attributes connected to the drivers
        :rtype: list[str]
        """

        pass

    def _drivenOutputs(self):
        """the output attributes of the constraint that are connected to the driven of the constraint
        Those attributes ares scanned to get the driven nodes through connection

        :return: the output attributes connected to the driven
        :rtype: list[str]
        """

        pass

    def _availableAttributes(self):
        """the attributes that are listed by the ``Node.attributes`` function

        :return: the available attributes
        :rtype: list[str]
        """

        pass

    @staticmethod
    def _formatDrivenAttributes(driven, drivenAttributes=None):
        """format the driven attributes

        :param driven: name of the object that will be driven by the constraint
        :type driven: str or :class:`cgp_maya_utils.scene.Node`

        :param drivenAttributes: the driven attributes to format
        :type drivenAttributes: list[:class:`cgp_maya_utils.constants.Transform`]

        :return: the formated drivenAttributes
        :rtype: list[str]
        """

        pass


class AimConstraint(Constraint):
    """node object that manipulates an ``aim`` constraint node
    """

    # ATTRIBUTES #

    _nodeType = 'aimConstraint'

    # COMMANDS #

    @classmethod
    def create(cls,
               drivers,
               driven,
               drivenAttributes=None,
               maintainOffset=False,
               weights=None,
               attributeValues=None,
               name=None,
               **__):
        """create an aimConstraint

        :param drivers: transforms driving the constraint
        :type drivers: list[str] or list[:class:`cgp_maya_utils.scene.Node`]

        :param driven: transform driven by the constraint
        :type driven: str or :class:`cgp_maya_utils.scene.Node`

        :param drivenAttributes: driven attributes controlled by the constraint - all attributes if nothing is specified
        :type drivenAttributes: list[:class:`cgp_maya_utils.constants.Transform`]

        :param maintainOffset: ``True`` : constraint created with offset - ``False`` : constraint created without offset
        :type maintainOffset: bool

        :param weights: values of the weight attributes of the constraint
        :type weights: list[int, float]

        :param attributeValues: attribute values to set on the constraint
        :type attributeValues: dict

        :param name: name of the constraint
        :type name: str

        :return: the created constraint
        :rtype: :class:`cgp_maya_utils.scene.AimConstraint`
        """

        pass

    # PRIVATE COMMANDS #

    def _availableAttributes(self):
        """the attributes that are listed by the ``Node.attributes`` function

        :return: the available attributes
        :rtype: list[str]
        """

        pass

    def _driverInputs(self):
        """get inputs

        :return: the inputs of the constraint related to the drivers
        :rtype: list[str]
        """

        pass

    def _drivenOutputs(self):
        """get the outputs of the constraint

        :return: the outputs of the constraint related to the driven
        :rtype: list[str]
        """

        pass


class OrientConstraint(Constraint):
    """node object that manipulates an ``orient`` constraint node
    """

    # ATTRIBUTES #

    _nodeType = 'orientConstraint'

    # COMMANDS #

    @classmethod
    def create(cls,
               drivers,
               driven,
               drivenAttributes=None,
               maintainOffset=False,
               weights=None,
               attributeValues=None,
               name=None,
               **__):
        """create an orientConstraint

        :param drivers: transforms driving the constraint
        :type drivers: list[str] or list[:class:`cgp_maya_utils.scene.Transform`]

        :param driven: transform driven by the constraint
        :type driven: str or :class:`cgp_maya_utils.scene.Transform`

        :param drivenAttributes: driven attributes controlled by the constraint - all attributes if nothing is specified
        :type drivenAttributes: list[:class:`cgp_maya_utils.constants.Transform`]

        :param maintainOffset: ``True`` : constraint created with offset - ``False`` : constraint created without offset
        :type maintainOffset: bool

        :param weights: values of the weight attributes of the constraint
        :type weights: list[int, float]

        :param attributeValues: attribute values to set on the constraint
        :type attributeValues: dict

        :param name: name of the constraint
        :type name: str

        :return: the created constraint
        :rtype: :class:`cgp_maya_utils.scene.OrientConstraint`
        """

        pass

    # PRIVATE COMMANDS #

    def _driverInputs(self):
        """get inputs

        :return: the inputs of the constraint related to the drivers
        :rtype: list[str]
        """

        pass

    def _drivenOutputs(self):
        """get the outputs of the constraint

        :return: the outputs of the constraint related to the driven
        :rtype: list[str]
        """

        pass

    def _availableAttributes(self):
        """the attributes that are listed by the ``Node.attributes`` function

        :return: the available attributes
        :rtype: list[str]
        """

        pass


class ParentConstraint(Constraint):
    """node object that manipulates an ``parent`` constraint node
    """

    # ATTRIBUTES #

    _nodeType = 'parentConstraint'

    # COMMANDS #

    @classmethod
    def create(cls,
               drivers,
               driven,
               drivenAttributes=None,
               maintainOffset=False,
               weights=None,
               attributeValues=None,
               name=None,
               **__):
        """create an parentConstraint

        :param drivers: transforms driving the constraint
        :type drivers: list[str] or list[:class:`cgp_maya_utils.scene.Transform`]

        :param driven: transform driven by the constraint
        :type driven: str or :class:`cgp_maya_utils.scene.Transform`

        :param drivenAttributes: driven attributes controlled by the constraint - all attributes if nothing is specified
        :type drivenAttributes: list[:class:`cgp_maya_utils.constants.Transform`]

        :param maintainOffset: ``True`` : constraint created with offset - ``False`` : constraint created without offset
        :type maintainOffset: bool

        :param weights: values of the weight attributes of the constraint
        :type weights: list[int, float]

        :param attributeValues: attribute values to set on the constraint
        :type attributeValues: dict

        :param name: name of the constraint
        :type name: str

        :return: the created constraint
        :rtype: :class:`cgp_may_utils.scene.ParentConstraint`
        """

        pass

    # PRIVATE COMMANDS #

    def _driverInputs(self):
        """get inputs

        :return: the inputs of the constraint related to the drivers
        :rtype: list[str]
        """

        pass

    def _drivenOutputs(self):
        """get the outputs of the constraint

        :return: the outputs of the constraint related to the driven
        :rtype: list[str]
        """

        pass

    def _availableAttributes(self):
        """the attributes that are listed by the ``Node.attributes`` function

        :return: the available attributes
        :rtype: list[str]
        """

        pass


class PointConstraint(Constraint):
    """node object that manipulates an ``point`` constraint node
    """

    # ATTRIBUTES #

    _nodeType = 'pointConstraint'

    # COMMANDS #

    @classmethod
    def create(cls,
               drivers,
               driven,
               drivenAttributes=None,
               maintainOffset=False,
               weights=None,
               attributeValues=None,
               name=None,
               **__):
        """create a pointConstraint

        :param drivers: transforms driving the constraint
        :type drivers: list[:class:`cgp_maya_utils.scene.Transform`]

        :param driven: transform driven by the constraint
        :type driven: str or :class:`cgp_maya_utils.scene.Transform`

        :param drivenAttributes: driven attributes controlled by the constraint - all attributes if nothing is specified
        :type drivenAttributes: list[:class:`cgp_maya_utils.constants.Transform`]

        :param maintainOffset: ``True`` : constraint created with offset - ``False`` : constraint created without offset
        :type maintainOffset: bool

        :param weights: values of the weight attributes of the constraint
        :type weights: list[int, float]

        :param attributeValues: attribute values to set on the constraint
        :type attributeValues: dict

        :param name: name of the constraint
        :type name: str

        :return: the created constraint
        :rtype: :class:`cgp_maya_utils.scene.PointConstraint`
        """

        pass

    # PRIVATE COMMANDS #

    def _driverInputs(self):
        """get inputs

        :return: the inputs of the constraint related to the drivers
        :rtype: list[str]
        """

        pass

    def _drivenOutputs(self):
        """get the outputs of the constraint

        :return: the outputs of the constraint related to the driven
        :rtype: list[str]
        """

        pass

    def _availableAttributes(self):
        """the attributes that are listed by the ``Node.attributes`` function

        :return: the available attributes
        :rtype: list[str]
        """

        pass


class ScaleConstraint(Constraint):
    """node object that manipulates an ``scale`` constraint node
    """

    # ATTRIBUTES #

    _nodeType = 'scaleConstraint'

    # COMMANDS #

    @classmethod
    def create(cls,
               drivers,
               driven,
               drivenAttributes=None,
               maintainOffset=False,
               weights=None,
               attributeValues=None,
               name=None,
               **__):
        """create a scaleConstraint

        :param drivers: transforms driving the constraint
        :type drivers: list[str] or list[:class:`cgp_maya_utils.scene.Transform`]

        :param driven: transform driven by the constraint
        :type driven: str or :class:`cgp_maya_utils.scene.Transform`

        :param drivenAttributes: driven attributes controlled by the constraint - all attributes if nothing is specified
        :type drivenAttributes: list[:class:`cgp_maya_utils.constants.Transform`]

        :param maintainOffset: ``True`` : constraint created with offset - ``False`` : constraint created without offset
        :type maintainOffset: bool

        :param weights: values of the weight attributes of the constraint
        :type weights: list[int, float]

        :param attributeValues: attribute values to set on the constraint
        :type attributeValues: dict

        :param name: name of the constraint
        :type name: str

        :return: the created constraint
        :rtype: :class:`cgp_maya_utils.scene.ScaleConstraint`
        """

        pass

    # PRIVATE COMMANDS #

    def _driverInputs(self):
        """get inputs

        :return: the inputs of the constraint related to the drivers
        :rtype: list[str]
        """

        pass

    def _drivenOutputs(self):
        """get the outputs of the constraint

        :return: the outputs of the constraint related to the driven
        :rtype: list[str]
        """

        pass

    def _availableAttributes(self):
        """the attributes that are listed by the ``Node.attributes`` function

        :return: the available attributes
        :rtype: list[str]
        """

        pass


class GeometryFilter(Node):
    """node object that manipulates any kind of geometry filter node
    """

    # ATTRIBUTES #

    _nodeType = 'geometryFilter'

    # OBJECT COMMANDS #

    @classmethod
    def create(cls, geometry, connections=None, attributeValues=None, name=None, **kwargs):
        """create a geometry filter

        :param geometry: geometry to bind the created geometry filter to
        :type geometry: str

        :param connections: connections to set on the created geometry filter
        :type connections: list[tuple[str]]

        :param attributeValues: attribute values to set on the created geometry filter
        :type attributeValues: dict

        :param name: name of the created geometry filter
        :type name: str

        :return: the created geometry filter object
        :rtype: :class:`cgp_maya_utils.scene.GeometryFilter`
        """

        pass

    # COMMANDS #

    def bind(self, shape):
        """bind the geometry filter to the specified shape

        :param shape: shape the geometry filter node will be bound to - can be the shape or its transform
        :type shape: str or :class:`cgp_maya_utils.scene.Shape` or :class:`cgp_maya_utils.scene.Transform`
        """

        pass

    def copy(self, shape, byProximity=True):
        """copy the geometry filter node to the shape

        :param shape: shape on which to copy the geometry filter node
        :type shape: str or :class:`cgp_maya_utils.scene.Shape`

        :param byProximity: ``True`` : weights are copied by proximity - ``False`` : weights are injected on points
        :type byProximity: bool
        """

        pass

    def data(self):
        """data necessary to store the geometry filter node on disk and/or recreate it from scratch

        :return: the data of the geometry filter node
        :rtype: dict
        """

        pass

    def reset(self):
        """reset the geometry filter node
        """

        pass

    def shapes(self):
        """the shapes deformed by the geometry filter node

        :return: the deformed shapes
        :rtype: list[:class:`cgp_maya_utils.scene.Shape`]
        """

        pass

    def weights(self):
        """the weights of the geometry filter node - same weights that are accessible through painting

        :return: the weights of the geometry filter node
        :rtype: any
        """

        pass

    # PRIVATE COMMANDS #

    def _availableAttributes(self):
        """the attributes that are listed by the ``Node.attributes`` function

        :return: the available attributes
        :rtype: list[str]
        """

        pass


class SkinCluster(GeometryFilter):
    """node object that manipulates a ``skinCluster`` node
    """

    # ATTRIBUTES #

    _nodeType = 'skinCluster'

    # OBJECT COMMANDS #

    @classmethod
    def create(cls, shapes, influences, weights=None, bindPreMatrixes=None, attributeValues=None, name=None, **__):
        """create a skinCluster

        :param shapes: shapes that will be deformed by the skinCluster
        :type shapes: list[str] or list[:class:`cgp_maya_utils.scene.Shape`]

        :param influences: influences that will drive the skinCluster
        :type influences: list[str] or list[:class:`cgp_maya_utils.scene.Joint`, :class:`cgp_maya_utils.scene.Shape`]

        :param weights: weights of the skin cluster - ``{influence1: [], influence2: [] ...}``
        :type weights: dict[str: list[int, float]]

        :param bindPreMatrixes: bindPreMatrixes of the skinCluster - ``{influence1: matrixAttribute1, influence2: matrixAttribute2 ...}``
        :type bindPreMatrixes: dict

        :param attributeValues: attribute values to set on the skinCluster node
        :type attributeValues: dict

        :param name: name of the skinCluster node
        :type name: str

        :return: the created skinCluster
        :rtype: :class:`cgp_maya_utils.scene.SkinCluster`
        """

        pass

    # COMMANDS #

    def addInfluence(self, influence):
        """add influence to the skinCluster

        :param influence: influence to add to the skinCluster
        :type influence: str
        """

        pass

    def bindPreMatrixes(self):
        """get the bindPreMatrixes of the skinCluster

        :return: bindPreMatrixes dictionary - ``{influence1: matrixAttribute1, influence2: matrixAttribute2 ...}``
        :rtype: dict[str: str]
        """

        pass

    def connectBindPreMatrixes(self, bindPreMatrixes=None):
        """connect the bindPreMatrixes of the skinCluster

        :param bindPreMatrixes: bindPreMatrixes of the skinCluster - ``{influence1: matrixAttribute1, influence2: matrixAttribute2 ...}``
        :type bindPreMatrixes: dict
        """

        pass

    def copy(self, shape, byProximity=True):
        """copy the skinCluster to the shape

        :param shape: shape on which to copy the skinCluster
        :type shape: str or :class:`cgp_maya_utils.scene.Shape`

        :param byProximity: ``True`` : weights are copied by proximity - ``False`` : weights are injected on points
        :type byProximity: bool

        :return: the created copied SkinCluster
        :rtype: :class:`cgp_maya_utils.scene.SkinCluster`
        """

        pass

    def data(self):
        """data necessary to store the skinCluster node on disk and/or recreate it from scratch

        :return: the data of the skinCluster node
        :rtype: dict
        """

        pass

    def influences(self):
        """the influences of the skinCluster

        :return: the influences of the skinCluster
        :rtype: list[:class:`cgp_maya_utils.scene.Joint`, :class:`cgp_maya_utils.scene.Shape`]
        """

        pass

    def removeInfluence(self, influence):
        """remove an influence from the skinCluster

        :param influence: the influence to remove from the skinCluster
        :type influence: str or :class:`cgp_maya_utils.scene.Joint` or :class:`cgp_maya_utils.scene.Shape`
        """

        pass

    def recacheBindMatrices(self):
        """recache the bind matrixes of the skinCluster
        """

        pass

    def reset(self):
        """reset the skinCluster
        """

        pass

    def setWeights(self, weights):
        """set the weights of the skinCluster

        :param weights: weights to set on the skinCluster - ``{influence1: [], influence2: [] ...}``
        :type weights: dict
        """

        pass

    def swapInfluences(self, oldFlag, newFlag, reset=True):
        """swap current influences by new influences using the flags

        :param oldFlag: flag that will be replaced by the new flag
        :type oldFlag: str

        :param newFlag: flag that will replace the old flag
        :type newFlag: str

        :param reset: ``True`` : skinCluster will be reset after the swap - ``False`` : skinCluster will not be reset
        :type reset: bool
        """

        pass

    def unusedInfluences(self):
        """the unused influences of the skinCluster

        :return: the unused influences
        :rtype: list[:class:`cgp_maya_utils.scene.Joint`, Shape]
        """

        pass

    def weights(self):
        """the weights of the geometry filter node - same weights that are accessible through painting

        :return: the weights of the skinCluster - ``{joint1: [], joint2: [] ...}``
        :rtype: dict
        """

        pass

    # PRIVATE COMMANDS #

    def _availableAttributes(self):
        """the attributes that are listed by the ``Node.attributes`` function

        :return: the available attributes
        :rtype: list[str]
        """

        pass


class Transform(DagNode):
    """node object that manipulates a ``transform`` node
    """

    # ATTRIBUTES #

    _nodeType = 'transform'

    # OBJECT COMMANDS #

    @classmethod
    def create(cls, translate=None, rotate=None, scale=None, rotateOrder=None, parent=None,
               worldSpace=False, connections=None, attributeValues=None, name=None, **__):
        """create a transform

        :param translate: translation values of the transform
        :type translate: list[int, float]

        :param rotate: rotation values  of the transform
        :type rotate: list[int, float]

        :param scale: scale values of the transform
        :type scale: list[int, float]

        :param rotateOrder: rotateOrder of the transform - default is ``cgp_maya_utils.constants.rotateOrder.XYZ``
        :type rotateOrder: str

        :param parent: parent of the transform
        :type parent: str or :class:`cgp_maya_utils.scene.DagNode`

        :param worldSpace: ``True`` : transform values are worldSpace - ``False`` : transform values are local
        :type worldSpace: bool

        :param connections: connections to set on the transform
        :type connections: list[tuple[str]]

        :param attributeValues: attribute values to set on the transform
        :type attributeValues: dict

        :param name: name of the transform
        :type name: str

        :return: the created transform
        :rtype: :class:`cgp_maya_utils.scene.Transform`
        """

        pass

    # COMMANDS #

    def constraints(self, constraintTypes=None, sources=True, destinations=True, constraintTypesIncluded=True):
        """the source constraints driving the node and the destination constraints the node drives

        :param constraintTypes: types of constraints to get - All if nothing is specified
        :type constraintTypes: list[str]

        :param sources: ``True`` : source constraints are returned  - ``False`` : source constraints are skipped
        :type sources: bool

        :param destinations: ``True`` : destination constraints returned -
                             ``False`` : destination constraints are skipped
        :type destinations: bool

        :param constraintTypesIncluded: ``True`` : constraint types are included -
                                        ``False`` : constraint types are excluded
        :type constraintTypesIncluded: bool

        :return: the constraints
        :rtype: list[:class:`cgp_maya_utils.scene.Constraint`,
                     :class:`cgp_maya_utils.scene.AimConstraint`,
                     :class:`cgp_maya_utils.scene.OrientConstraint`,
                     :class:`cgp_maya_utils.scene.ParentConstraint`,
                     :class:`cgp_maya_utils.scene.PointConstraint`,
                     :class:`cgp_maya_utils.scene.ScaleConstraint`,]
        """

        pass

    def data(self, worldSpace=False):
        """data necessary to store the transform node on disk and/or recreate it from scratch

        :param worldSpace: ``True`` : transform values are worldSpace -
                           ``False`` : transform values are local
        :type worldSpace: bool

        :return: the data of the transform
        :rtype: dict
        """

        pass

    def duplicate(self, withChildren=True):
        """duplicate the transform node

        :param withChildren: ``True`` : transform node is duplicated with its children -
                             ``False`` : transform node is not duplicated with its children
        :type withChildren: bool

        :return: the duplicated transform
        :rtype: :class:`cgp_maya_utils.scene.Transform`
        """

        pass

    def freeze(self, translate=True, rotate=True, scale=True, normal=False):
        """freeze the values of the transform

        :param translate: ``True`` : translation values are frozen - ``False`` : translation values are not frozen
        :type translate: bool

        :param rotate: ``True`` : rotation values are frozen - ``False`` : rotation values are not frozen
        :type rotate: bool

        :param scale: ``True`` : scale values are frozen - ``False`` : scale values are not frozen
        :type scale: bool

        :param normal: ``True`` : normal values are frozen - ``False`` : normal values are not frozen
        :type normal: bool
        """

        pass

    def match(self, targetTransform, attributes=None, worldSpace=True):
        """match the transform to the target transform

        :param targetTransform: transform to match to
        :type targetTransform: str or :class:`cgp_maya_utils.scene.Transform`

        :param attributes: attributes to match - default is ``cgp_maya_utils.constants.Transform.GENERAL``
        :type attributes: list[str]

        :param worldSpace: ``True`` : match occurs in worldSpace - ``False`` : match occurs in local
        :type worldSpace: bool
        """

        pass

    def mirror(self, mirrorPlane=None, attributes=None, worldSpace=False, mode=None):
        """mirror the transform

        :param mirrorPlane: plane of mirroring - default is ``cgp_generic_utils.constants.MirrorPlane.YZ``
        :type mirrorPlane: str

        :param attributes: transform attributes to mirror - default is ``cgp_maya_utils.constants.Transform.GENERAL``
        :type attributes: list

        :param worldSpace: ``True`` : mirror occurs in worldSpace - ``False`` : mirror occurs in local
        :type worldSpace: bool

        :param mode: mode of mirroring - default is ``cgp_generic_utils.constants.MirrorMode.MIRROR``
        :type mode: str
        """

        pass

    @cgp_maya_utils.decorators.KeepCurrentSelection()
    def mirrorTransformValues(self, mirrorPlane=None, worldSpace=False, rotateOrder=None, mode=None):
        """the mirror transform values

        :param mirrorPlane: plane of mirroring - default is ``cgp_generic_utils.constants.MirrorPlane.YZ``
        :type mirrorPlane: str

        :param worldSpace: ``True`` : mirror transform values are worldSpace -
                           ``False`` : mirror transform values are local
        :type worldSpace: bool

        :param rotateOrder: rotateOrder to get the mirror transform values in - ! ONLY IN WORLDSPACE !
        :type rotateOrder: str

        :param mode: mode of mirroring - default is ``cgp_generic_utils.constants.MirrorMode.MIRROR``
        :type mode: str

        :return: the mirrored transforms
        :rtype: dict
        """

        pass

    def reset(self, translate=True, rotate=True, scale=True, shear=True):
        """reset the values of the transform to its default values

        :param translate: ``True`` : translation values are reset - ``False`` : translation values are not reset
        :type translate: bool

        :param rotate: ``True`` : rotation values are reset - ``False`` : rotation values are not reset
        :type rotate: bool

        :param scale: ``True`` : scale values are reset - ``False`` : scale values are not reset
        :type scale: bool

        :param shear: ``True`` : shear values are reset - ``False`` : shear values are not reset
        :type shear: bool
        """

        pass

    def rotate(self, x=None, y=None, z=None, worldSpace=False, mode=None):
        """rotate the transform

        :param x: value of rotateX to set
        :type x: float

        :param y: value of rotateY to set
        :type y: float

        :param z: value of rotateZ to set
        :type z: float

        :param worldSpace: ``True`` : rotate occurs in worldSpace - ``False`` : rotate occurs in local
        :type worldSpace: bool

        :param mode: ``cgp_generic_utils.constants.TransformMode.ABSOLUTE`` : value is replaced, default mode
                     ``cgp_generic_utils.constants.TransformMode.RELATIVE`` : value is added
        :type mode: str
        """

        pass

    def scale(self, x=None, y=None, z=None, mode=None):
        """scale the transform

        :param x: value of scaleX to set
        :type x: float

        :param y: value of scaleY to set
        :type y: float

        :param z: value of scaleZ to set
        :type z: float

        :param mode: ``cgp_generic_utils.constants.TransformMode.ABSOLUTE`` : value is replaced  default mode
                     ``cgp_generic_utils.constants.TransformMode.RELATIVE`` : value is added
        :type mode: str
        """

        pass

    def shear(self, xy=None, xz=None, yz=None, mode=None):
        """shear the transform

        :param xy: value of shearXY to set
        :type xy: float

        :param xz: value of shearXZ to set
        :type xz: float

        :param yz: value of shearYZ to set
        :type yz: float

        :param mode: ``cgp_generic_utils.constants.TransformMode.ABSOLUTE`` : value is replaced, default mode -
                     ``cgp_generic_utils.constants.TransformMode.RELATIVE`` : value is added
        :type mode: str
        """

        pass

    def setTransformValues(self, transformValues, worldSpace=False):
        """set transform values

        :param transformValues: transform values to set
        :type transformValues: dict

        :param worldSpace: ``True`` : values are set in worldSpace - ``False`` : values are set in local
        :type worldSpace: bool
        """

        pass

    def shapes(self, shapeTypes=None, shapeTypesIncluded=True):
        """the shapes parented under the transform

        :param shapeTypes: types of shape to get - All if nothing is specified
        :type shapeTypes: list[str]

        :param shapeTypesIncluded: ``True`` : shape types are included - ``False`` : shape types are excluded
        :type shapeTypesIncluded: bool

        :return: the shapes of the transform
        :rtype: list[:class:`cgp_maya_utils.scene.Shape`,
                     :class:`cgp_maya_utils.scene.NurbsCurve`,
                     :class:`cgp_maya_utils.scene.NurbsSurface`,
                     :class:`cgp_maya_utils.scene.Mesh`,]
        """

        pass

    def transformationMatrix(self, worldSpace=False, rotateOrder=None):
        """the transformationMatrix of the transform

        :param worldSpace: ``True`` : TransformationMatrix is initialized with the worldSpace transform values -
                           ``False`` : TransformationMatrix is initialized with the local transform values
        :type worldSpace: bool

        :param rotateOrder: rotateOrder to get the transform values in -
                            default is the current rotateOrder of the transform
        :type rotateOrder: str

        :return: the transformMatrix
        :rtype: :class:`cgp_generic_utils.api.TransformationMatrix`
        """

        pass

    def transformValues(self, worldSpace=False, rotateOrder=None):
        """the transform values

        :param worldSpace: ``True`` : transform values are worldSpace - ``False`` : transform values are local
        :type worldSpace: bool

        :param rotateOrder: rotateOrder to get the transform values in ! ONLY IN WORLDSPACE !
        :type rotateOrder: str

        :return: the transform values
        :rtype: dict
        """

        pass

    def translate(self, x=None, y=None, z=None, worldSpace=False, mode=None):
        """translate the transform

        :param x: value of translateX to set
        :type x: float

        :param y: value of translateX to set
        :type y: float

        :param z: value of translateX to set
        :type z: float

        :param worldSpace: ``True`` : translation occurs in worldSpace - ``False`` : translation occurs in local
        :type worldSpace: bool

        :param mode: ``cgp_generic_utils.constants.TransformMode.ABSOLUTE`` : value is replaced, default mode -
                     ``cgp_generic_utils.constants.TransformMode.RELATIVE`` : value is added
        :type mode: str
        """

        pass

    # PRIVATE COMMANDS #

    def _availableAttributes(self):
        """the attributes that are listed by the ``Node.attributes`` function

        :return: the available attributes
        :rtype: list[str]
        """

        pass

    @staticmethod
    def _formatAttributes(attributes):
        """format the transform attributes into a formated data list

        :param attributes: attributes to format
        :type attributes: list[str]

        :return: the formated attributes
        :rtype: list[str]
        """

        pass

    @cgp_maya_utils.decorators.KeepCurrentSelection()
    def _setTransformValues(self, values, attributes=None, worldSpace=False):
        """set transform values from the dictionary to the specified object

        :param values: the dictionary of the transform values to set on the specified object
        :type values: dict

        :param attributes: transform attributes to set - default is ``cgp_maya_utils.constants.Transform.GENERAL``
        :type attributes: list[str]

        :param worldSpace: ``True`` : transform values are set on worldSpace -
                           ``False`` : transform values are set on local
        :type worldSpace: bool
        """

        pass


class Joint(Transform):
    """node object that manipulates a ``joint`` node
    """

    # ATTRIBUTES #

    _nodeType = 'joint'

    # PRIVATE COMMANDS #

    def _availableAttributes(self):
        """the attributes that are listed by the ``Node.attributes`` function

        :return: the available attributes
        :rtype: list[str]
        """

        pass


class IkEffector(Transform):
    """node object that manipulates an ``ikEffector`` node
    """

    # ATTRIBUTES #

    _nodeType = 'ikEffector'


class IkHandle(Transform):
    """node object that manipulates an ``ikHandle`` node
    """

    # ATTRIBUTES #

    _nodeType = 'ikHandle'

    # OBJECT COMMANDS #

    @classmethod
    def create(cls, startJoint, endJoint, solverType=None, connections=None,
               attributeValues=None, name=None, **__):
        """create an ik handle

        :param startJoint: start joint of the ik handle
        :type startJoint: str or :class:`cgp_maya_utils.scene.Joint`

        :param endJoint: end joint of the ik handle
        :type endJoint: str or :class:`cgp_maya_utils.scene.Joint`

        :param solverType: type of solver of the ik handle - default is ``cgp_maya_utils.constants.Solver.IK_RP_SOLVER``
        :type solverType: str

        :param connections: connections to set on the ikHandle
        :type connections: list[tuple[str]]

        :param attributeValues: attribute values to set on the ikHandle node
        :type attributeValues: dict

        :param name: name of the node
        :type name: str

        :return: the created ik handle
        :rtype: :class:`cgp_maya_utils.scene.IkHandle`
        """

        pass

    # COMMANDS #

    def data(self, worldSpace=False):
        """data necessary to store the ik handle node on disk and/or recreate it from scratch

        :param worldSpace: ``True`` : ikHandle transforms are in worldSpace - ``False`` : ikHandle transforms in local
        :type worldSpace: bool

        :return: the data of the ik handle
        :rtype: dict
        """

        pass

    def effector(self):
        """the effector of the ik handle

        :return: the effector of the ik handle
        :rtype: :class:`cgp_maya_utils.scene.IkEffector`
        """

        pass

    def endJoint(self):
        """the end joint of the ik handle

        :return: the end joint of the ik handle
        :rtype: :class:`cgp_maya_utils.scene.Joint`
        """

        pass

    def setSolver(self, solverType):
        """set the solver of the ik handle

        :param solverType: type of solver to set
        :type solverType: str
        """

        pass

    def startJoint(self):
        """the start joint of the ik handle

        :return: the start joint
        :rtype: :class:`cgp_maya_utils.scene.Joint`
        """

        pass


class Shape(DagNode):
    """node object that manipulates any kind of shape node
    """

    # ATTRIBUTES #

    _nodeType = 'shape'
    _inputGeometry = None
    _outputGeometry = None
    _library = None

    # OBJECT COMMANDS #

    @classmethod
    def import_(cls, style, parent=None, name=None):
        """import a shape

        :param style: style of the shape to import that exists in the shape library - ex : ``cube`` - ``circle``
        :type style: str

        :param parent: transform to which the shape will be parented - new transform if nothing specified
        :type parent: str or :class:`cgp_maya_utils.scene.Transform`

        :param name: name of the shape
        :type name: str

        :return: the imported shape
        :rtype: :class:`cgp_maya_utils.scene.Shape`
        """

        pass

    # COMMANDS #

    def count(self):
        """the count of points

        :return: the count of points
        :rtype: int
        """

        pass

    def data(self, worldSpace=False):
        """data necessary to store the shape node on disk and/or recreate it from scratch

        :param worldSpace: ``True`` : shape position is in worldSpace - ``False`` : shape position is in local
        :type worldSpace: bool

        :return: the data of the shape
        :rtype: dict
        """

        pass

    def duplicate(self, newTransform=False):
        """duplicate the shape

        :param newTransform: ``True`` : duplicated shape has a new transform - ``False`` duplicate shape has the same transform
        :type newTransform: bool

        :return: the duplicated shape
        :rtype: :class:`cgp_maya_utils.Shape`
        """

        pass

    def export(self, name):
        """export the shape in the library

        :param name: name of the shape in the library
        :type name: str

        :return: the exported file
        :rtype: :class:`cgp_generic_utils.files.JsonFile`
        """

        pass

    def geometryFilters(self, geometryFilterTypes=None, geometryFilterTypesIncluded=True):
        """the geometryFilters bounded to the shape

        :param geometryFilterTypes: types of geometryFilters to get - All if nothing is specified
        :type geometryFilterTypes: list[str]

        :param geometryFilterTypesIncluded: ``True`` : geometryFilter types are included - ``False`` : geometryFilter types are excluded
        :type geometryFilterTypesIncluded: bool

        :return: the geometryFilters bound to the shape
        :rtype: list[:class:`cgp_maya_utils.scene.GeometryFilter`]
        """

        pass

    def isDeformed(self):
        """check if the shape is deformed

        :return: ``True`` : the shape is deformed - ``False`` : the shape is not deformed
        :rtype: bool
        """

        pass

    def match(self, targetShape, worldSpace=False):
        """match the shape to the target shape

        :param targetShape: shape to match the current shape to
        :type targetShape: str or :class:`cgp_maya_utils.scene.Shape`

        :param worldSpace: ``True`` : match is in worldSpace - ``False`` : match is in local
        :type worldSpace: bool
        """

        pass

    def mirror(self, mirrorPlane=None, worldSpace=False):
        """mirror the shape

        :param mirrorPlane: plane used to perform the mirror - default is ``cgp_generic_utils.constants.MirrorPlane.YZ``
        :type mirrorPlane: str

        :param worldSpace: ``True`` : mirror is in worldSpace - ``False`` : mirror is in local
        :type worldSpace: bool
        """

        pass

    def mirroredPositions(self, mirrorPlane=None, worldSpace=False):
        """the mirror positions of the points of the shape

        :param mirrorPlane: plane used to perform the mirror - default is ``cgp_generic_utils.constants.MirrorPlane.YZ``
        :type mirrorPlane: str

        :param worldSpace: ``True`` : positions are worldSpace - ``False`` : positions are local
        :type worldSpace: bool

        :return: the mirrored positions of the points
        :rtype: list[list[float]]
        """

        pass

    def points(self):
        """the points of the shape

        :return: the points of the shape
        :rtype: list[str]
        """

        pass

    def positions(self, worldSpace=False):
        """the positions of the points of the shape

        :param worldSpace: ``True`` : positions are worldSpace - ``False`` : positions are local
        :type worldSpace: bool

        :return: the positions of the points
        :rtype: list[list[float]]
        """

        pass

    def rotate(self, values, worldSpace=False, aroundBoundingBoxCenter=False):
        """rotate the shape

        :param values: values used to rotate the shape
        :type values: list[int, float]

        :param worldSpace: ``True`` : rotation is in worldSpace - ``False`` : rotation is in local
        :type worldSpace: bool

        :param aroundBoundingBoxCenter: ``True`` : rotation around bounding box center - ``False`` : rotation around objet pivot
        :type aroundBoundingBoxCenter: bool
        """

        pass

    def scale(self, values, aroundBoundingBoxCenter=False):
        """scale the shape

        :param values: values used to scale the shape
        :type values: list[int, float]

        :param aroundBoundingBoxCenter: If False shape will rotate around obj pivot. Otherwise around boundingBox center
        :type aroundBoundingBoxCenter: bool
        """

        pass

    def setColor(self, color=None):
        """set the color of the shape

        :param color: color channel values to set to the shape - ``indexValue`` or ``[r, g, b]``
        :type color: int or list[int]
        """

        pass

    def setPositions(self, positions, worldSpace=False):
        """set the positions of the points of the shape

        :param positions: positions to set - ``[[x1, y1, z1], [x2, y2, z2], ...]`` - len(positions) = self.count()
        :type positions: list[list[int, float]]

        :param worldSpace: ``True`` : positions are set in worldSpace - ``False`` : positions are set in local
        :type worldSpace: bool
        """

        pass

    def setTransform(self, transform, worldSpace=False, deleteOriginalTransform=False):
        """set the transform of the shape

        :param transform: transform the shape will be parented to
        :type transform: str or :class:`cgp_maya_utils.scene.Transform`

        :param worldSpace: ``True`` : parenting occurs in worldSpace - ``False`` : parenting occurs in local
        :type worldSpace: bool

        :param deleteOriginalTransform: ``True`` : original transform is deleted - ``False`` : original transform remains
        :type deleteOriginalTransform: bool
        """

        pass

    def transform(self):
        """the transform of the shape

        :return: the transform of the shape
        :rtype: :class:`cgp_maya_utils.scene.Transform` or :class:`cgp_maya_utils.scene.Joint`
        """

        pass

    def translate(self, values, worldSpace=False):
        """translate the shape

        :param values: values used to translate the shape
        :type values: list[int, float]

        :param worldSpace: ``True`` : translation is in worldSpace - ``False`` : translation is in local
        :type worldSpace: bool
        """

        pass


class NurbsCurve(Shape):
    """node object that manipulates a ``nurbsCurve`` shape node
    """

    # ATTRIBUTES #

    _nodeType = 'nurbsCurve'
    _inputGeometry = 'create'
    _outputGeometry = 'local'
    _library = cgp_maya_utils.constants.Environment.NURBS_CURVE_LIBRARY

    # OBJECT COMMANDS #

    @classmethod
    def create(cls, positions, transform=None, form=None, degree=None, color=None,
               worldSpace=False, attributeValues=None, connections=None, name=None, **__):
        """create a nurbsCurve

        :param positions: positions of the points of the nurbsCurve
        :type positions: list[list[int, float]]

        :param transform: transform under which the shape will be parented
        :type transform: str or :class:`cgp_maya_utils.scene.Transform`

        :param form: form of the nurbsCurve - default is ``cgp_maya_utils.constants.ShapeFormType.OPEN``
        :type form: str

        :param degree: degree of the nurbsCurve - default is ``cgp_maya_utils.constants.GeometryDegree.CUBIC``
        :type degree: str

        :param color: color of the nurbsCurve
        :type color: list[int, float]

        :param worldSpace: ``True`` : creation is in worldSpace - ``False`` : creation is in local
        :type worldSpace: bool

        :param attributeValues: attribute values to set on the nurbsCurve
        :type attributeValues: dict

        :param connections: connections to set on the nurbsCurve
        :type connections: list[tuple[str]]

        :param name: name of the nurbsSurface
        :type name: str

        :return: the created nurbsCurve
        :rtype: :class:`cgp_maya_utils.scene.NurbsCurve`
        """

        pass

    # COMMANDS #

    def count(self):
        """the count of cv points

        :return: the count of cv points
        :rtype: int
        """

        pass

    def data(self, worldSpace=False):
        """data necessary to store the nurbsCurve node on disk and/or recreate it from scratch

        :param worldSpace: ``True`` : shape position is in worldSpace - ``False`` : shape position is in local
        :type worldSpace: bool

        :return: the data of the nurbsCurve
        :rtype: dict
        """

        pass

    def isOpened(self):
        """check if the curve is opened or closed

        :return: ``True`` : shape is opened - ``False`` : shape is closed
        :rtype: bool
        """

        pass

    def points(self):
        """the cv points of the nurbsCurve

        :return: the cv points of the nurbsCurve
        :rtype: list[str]
        """

        pass


class NurbsSurface(Shape):
    """node object that manipulates a ``nurbsSurface`` shape node
    """

    # ATTRIBUTES #

    _nodeType = 'nurbsSurface'
    _inputGeometry = 'create'
    _outputGeometry = 'local'
    _library = cgp_maya_utils.constants.Environment.NURBS_SURFACE_LIBRARY

    # OBJECT COMMANDS #

    @classmethod
    def create(cls, positions, transform=None, formU=None, formV=None, degreeU=None, degreeV=None, knotsU=None,
               knotsV=None, color=None, worldSpace=False, attributeValues=None, connections=None, name=None, **__):
        """create a nurbsSurface

        :param positions: positions of the points of the nurbsSurface
        :type positions: list[list[int, float]]

        :param transform: transform under which the shape will be parented
        :type transform: str or :class:`cgp_maya_utils.scene.Transform`

        :param formU: formU of the nurbsSurface - default is ``cgp_maya_utils.constants.ShapeFormType.OPEN``
        :type formU: str

        :param formV: formU of the nurbsSurface - default is ``cgp_maya_utils.constants.ShapeFormType.OPEN``
        :type formV: str

        :param degreeU: degreeU of the nurbsSurface - default is ``cgp_maya_utils.constants.GeometryDegree.CUBIC``
        :type degreeU: str

        :param degreeV: degreeU of the nurbsSurface - default is ``cgp_maya_utils.constants.GeometryDegree.CUBIC``
        :type degreeV: str

        :param knotsU: knotU of the nurbsSurface
        :type knotsU: list[int]

        :param knotsV: knotV of the nurbsSurface
        :type knotsV: list[int]

        :param color: color of the nurbsSurface
        :type color: list[int, float]

        :param worldSpace: ``True`` : creation is in worldSpace - ``False`` : creation is in local
        :type worldSpace: bool

        :param attributeValues: attribute values to set on the nurbsSurface
        :type attributeValues: dict

        :param connections: connections to set on the nurbsSurface
        :type connections: list[tuple[str]]

        :param name: name of the nurbsSurface
        :type name: str

        :return: the created nurbsSurface
        :rtype: :class:`cgp_maya_utils.scene.NurbsSurface`
        """

        pass

    # COMMANDS #

    def count(self):
        """the count of cv points

        :return: the count of cv points
        :rtype: int
        """

        pass

    def countU(self):
        """the countU of cv points

        :return: the countU of cv points
        :rtype: int
        """

        pass

    def countV(self):
        """the countV of cv points

        :return: the countV of cv points
        :rtype: int
        """

        pass

    def data(self, worldSpace=False):
        """data necessary to store the nurbsSurface node on disk and/or recreate it from scratch

        :param worldSpace: ``True`` : shape position is in worldSpace - ``False`` : shape position is in local
        :type worldSpace: bool

        :return: the data of the nurbsSurface
        :rtype: dict
        """

        pass

    def formU(self):
        """the formU of the nurbsSurface

        :return: the formU of the nurbsSurface
        :rtype: int
        """

        pass

    def formV(self):
        """the formV of the nurbsSurface

        :return: the formV of the nurbsSurface
        :rtype: int
        """

        pass

    def knotsU(self):
        """the knotU of the nurbsSurface

        :return: the knotU of the nurbsSurface
        :rtype: list[int]
        """

        pass

    def knotsV(self):
        """the knotV of the nurbsSurface

        :return: the knotV of the nurbsSurface
        :rtype: list[int]
        """

        pass

    def points(self):
        """the cv points of the nurbsSurface

        :return: the cv points of the nurbsSurface
        :rtype: list[str]
        """

        pass


class Mesh(Shape):
    """node object that manipulates a ``mesh`` shape node
    """

    # ATTRIBUTES #

    _nodeType = 'mesh'
    _inputGeometry = 'inMesh'
    _outputGeometry = 'outMesh'
    _library = cgp_maya_utils.constants.Environment.MESH_LIBRARY

    # OBJECT COMMANDS #

    @classmethod
    def import_(cls, style, parent=None, name=None):
        """import a mesh

        :param style: style of the mesh shape to import that exists in the shape library - ex : ``cube`` - ``circle``
        :type style: str

        :param parent: transform to which the mesh will be parented - new transform if nothing specified
        :type parent: str or :class:`cgp_maya_utils.scene.Transform`

        :param name: name of the mesh
        :type name: str

        :return: the imported mesh
        :rtype: :class:`cgp_maya_utils.scene.Mesh`
        """

        pass

    # COMMANDS #

    def count(self):
        """the count of vertices of the mesh

        :return: the count of vertices of the mesh
        :rtype: int
        """

        pass

    @cgp_maya_utils.decorators.KeepCurrentSelection()
    def export(self, name):
        """export the mesh in the library

        :param name: name of the mesh in the library
        :type name: str

        :return: the exported file
        :rtype: :class:`cgp_maya_utils.files.ObjFile`
        """

        pass

    def points(self):
        """the vertices of the mesh

        :return: the vertices of the mesh
        :rtype: list[str]
        """

        pass


def attribute(fullName):
    """the attribute object from an attribute full name

    :param fullName: the full name of the attribute - ``node.attribute``
    :type fullName: str

    :return: the attribute object
    :rtype: :class:`cgp_maya_utils.scene.Attribute`,
            :class:`cgp_maya_utils.scene.BoolAttribute`,
            :class:`cgp_maya_utils.scene.ByteAttribute`,
            :class:`cgp_maya_utils.scene.CompoundAttribute`,
            :class:`cgp_maya_utils.scene.Double3Attribute`,
            :class:`cgp_maya_utils.scene.DoubleAngleAttribute`,
            :class:`cgp_maya_utils.scene.DoubleAttribute`,
            :class:`cgp_maya_utils.scene.DoubleLinearAttribute`,
            :class:`cgp_maya_utils.scene.EnumAttribute`,
            :class:`cgp_maya_utils.scene.Float3Attribute`,
            :class:`cgp_maya_utils.scene.FloatAttribute`,
            :class:`cgp_maya_utils.scene.LongAttribute`,
            :class:`cgp_maya_utils.scene.MatrixAttribute`,
            :class:`cgp_maya_utils.scene.MessageAttribute`,
            :class:`cgp_maya_utils.scene.NumericAttribute`,
            :class:`cgp_maya_utils.scene.ShortAttribute`,
            :class:`cgp_maya_utils.scene.StringAttribute`,
            :class:`cgp_maya_utils.scene.TDataCompoundAttribute`,
            :class:`cgp_maya_utils.scene.TimeAttribute`
    """

    pass


def connection(source, destination):
    """the connection object representing a connection, live or virtual, between the source and the destination

    :param source: the source of the connection
    :type source: str or :class:`cgp_maya_utils.scene.Attribute`

    :param destination: the destination of the connection
    :type destination: str or :class:`cgp_maya_utils.scene.Attribute`

    :return: the connection object
    :rtype: :class:`cgp_maya_utils.scene.Connection`
    """

    pass


def createAttribute(data, **extraData):
    """create an attribute using an attribute data dictionary

    :param data: data used to create the attribute
    :type data: dict

    :param extraData: extraData used to update the data dictionary before creating the attribute
    :type extraData: any

    :return: the created attribute
    :rtype: :class:`cgp_maya_utils.scene.Attribute`,
            :class:`cgp_maya_utils.scene.BoolAttribute`,
            :class:`cgp_maya_utils.scene.ByteAttribute`,
            :class:`cgp_maya_utils.scene.CompoundAttribute`,
            :class:`cgp_maya_utils.scene.Double3Attribute`,
            :class:`cgp_maya_utils.scene.DoubleAngleAttribute`,
            :class:`cgp_maya_utils.scene.DoubleAttribute`,
            :class:`cgp_maya_utils.scene.DoubleLinearAttribute`,
            :class:`cgp_maya_utils.scene.EnumAttribute`,
            :class:`cgp_maya_utils.scene.Float3Attribute`,
            :class:`cgp_maya_utils.scene.FloatAttribute`,
            :class:`cgp_maya_utils.scene.LongAttribute`,
            :class:`cgp_maya_utils.scene.MatrixAttribute`,
            :class:`cgp_maya_utils.scene.MessageAttribute`,
            :class:`cgp_maya_utils.scene.NumericAttribute`,
            :class:`cgp_maya_utils.scene.ShortAttribute`,
            :class:`cgp_maya_utils.scene.StringAttribute`,
            :class:`cgp_maya_utils.scene.TDataCompoundAttribute`,
            :class:`cgp_maya_utils.scene.TimeAttribute`
    """

    pass


def getAttributes(name, attributeTypes=None):
    """get the existing attributes in the scene

    :param name: name of the attributes to get in the scene
    :type name: str

    :param attributeTypes: types of attributes to get in the scene
    :type attributeTypes: list[str]

    :return: the listed Attributes
    :rtype: tuple[:class:`cgp_maya_utils.scene.Attribute`,
                  :class:`cgp_maya_utils.scene.BoolAttribute`,
                  :class:`cgp_maya_utils.scene.ByteAttribute`,
                  :class:`cgp_maya_utils.scene.CompoundAttribute`,
                  :class:`cgp_maya_utils.scene.Double3Attribute`,
                  :class:`cgp_maya_utils.scene.DoubleAngleAttribute`,
                  :class:`cgp_maya_utils.scene.DoubleAttribute`,
                  :class:`cgp_maya_utils.scene.DoubleLinearAttribute`,
                  :class:`cgp_maya_utils.scene.EnumAttribute`,
                  :class:`cgp_maya_utils.scene.Float3Attribute`,
                  :class:`cgp_maya_utils.scene.FloatAttribute`,
                  :class:`cgp_maya_utils.scene.LongAttribute`,
                  :class:`cgp_maya_utils.scene.MatrixAttribute`,
                  :class:`cgp_maya_utils.scene.MessageAttribute`,
                  :class:`cgp_maya_utils.scene.NumericAttribute`,
                  :class:`cgp_maya_utils.scene.ShortAttribute`,
                  :class:`cgp_maya_utils.scene.StringAttribute`,
                  :class:`cgp_maya_utils.scene.TDataCompoundAttribute`,
                  :class:`cgp_maya_utils.scene.TimeAttribute`]
    """

    pass


def getNodesFromAttributes(attributes):
    """get the nodes having the specified attributes

    :param attributes: attributes required on the node for it to be part of the return
    :type attributes: list[str]

    :return: the gathered nodes
    :rtype: tuple[:class:`cgp_maya_utils.scene.Node`,
                  :class:`cgp_maya_utils.scene.AimConstraint`,
                  :class:`cgp_maya_utils.scene.AnimCurve`,
                  :class:`cgp_maya_utils.scene.AnimCurveTA`,
                  :class:`cgp_maya_utils.scene.AnimCurveTL`,
                  :class:`cgp_maya_utils.scene.AnimCurveTU`,
                  :class:`cgp_maya_utils.scene.Constraint`,
                  :class:`cgp_maya_utils.scene.DagNode`,
                  :class:`cgp_maya_utils.scene.GeometryFilter`,
                  :class:`cgp_maya_utils.scene.IkEffector`,
                  :class:`cgp_maya_utils.scene.IkHandle`,
                  :class:`cgp_maya_utils.scene.Joint`,
                  :class:`cgp_maya_utils.scene.Mesh`,
                  :class:`cgp_maya_utils.scene.Node`,
                  :class:`cgp_maya_utils.scene.NurbsCurve`,
                  :class:`cgp_maya_utils.scene.NurbsSurface`,
                  :class:`cgp_maya_utils.scene.OrientConstraint`,
                  :class:`cgp_maya_utils.scene.ParentConstraint`,
                  :class:`cgp_maya_utils.scene.PointConstraint`,
                  :class:`cgp_maya_utils.scene.Reference`,
                  :class:`cgp_maya_utils.scene.ScaleConstraint`,
                  :class:`cgp_maya_utils.scene.Shape`,
                  :class:`cgp_maya_utils.scene.SkinCluster`,
                  :class:`cgp_maya_utils.scene.Transform`]
    """

    pass


# COMPONENT COMMANDS #


def component(fullName):
    """the component object from a component full name

    :param fullName: the full name of the component - ``shape.component[]`` or ``shape.component[][]``
    :type fullName: str

    :return: the component object
    :rtype: :class:`cgp_maya_utils.scene.Component`,
            :class:`cgp_maya_utils.scene.CurvePoint`,
            :class:`cgp_maya_utils.scene.Edge`,
            :class:`cgp_maya_utils.scene.EditPoint`,
            :class:`cgp_maya_utils.scene.Face`,
            :class:`cgp_maya_utils.scene.IsoparmU`,
            :class:`cgp_maya_utils.scene.IsoparmV`,
            :class:`cgp_maya_utils.scene.SurfacePatch`,
            :class:`cgp_maya_utils.scene.SurfacePoint`,
            :class:`cgp_maya_utils.scene.TransformComponent`,
            :class:`cgp_maya_utils.scene.Vertex`,
    """

    pass


def currentNamespace(asAbsolute=True):
    """the current namespace of the maya scene

    :param asAbsolute: defines whether or not the command will return an absolute namespace
    :type asAbsolute: bool

    :return: the current namespace object
    :rtype: :class:`cgp_maya_utils.scene.Namespace`
    """

    pass


def namespace(name):
    """the namespace object from a name

    :param name: name to get the namespace object from
    :type name: str

    :return: the namespace object
    :rtype: :class:`cgp_maya_utils.scene.Namespace`
    """

    pass


def plugin(name):
    """the plugin object from a name

    :param name: name to get the plugin object from
    :type name: str

    :return: the plugin object
    :rtype: :class:`cgp_maya_utils.scene.Plugin`
    """

    pass


def scene():
    """the scene object from the live maya scene

    :return: the scene object
    :rtype: :class:`cgp_maya_utils.scene.Scene`
    """

    pass


def createNode(data, **extraData):
    """create a node using a node data dictionary

    :param data: data used to create the node
    :type data: dict

    :param extraData: extraData used to update the data before creating the node
    :type extraData: any

    :return: the created node
    :rtype: :class:`cgp_maya_utils.scene.Node`,
            :class:`cgp_maya_utils.scene.AimConstraint`,
            :class:`cgp_maya_utils.scene.AnimCurve`,
            :class:`cgp_maya_utils.scene.AnimCurveTA`,
            :class:`cgp_maya_utils.scene.AnimCurveTL`,
            :class:`cgp_maya_utils.scene.AnimCurveTU`,
            :class:`cgp_maya_utils.scene.Constraint`,
            :class:`cgp_maya_utils.scene.DagNode`,
            :class:`cgp_maya_utils.scene.GeometryFilter`,
            :class:`cgp_maya_utils.scene.IkEffector`,
            :class:`cgp_maya_utils.scene.IkHandle`,
            :class:`cgp_maya_utils.scene.Joint`,
            :class:`cgp_maya_utils.scene.Mesh`,
            :class:`cgp_maya_utils.scene.Node`,
            :class:`cgp_maya_utils.scene.NurbsCurve`,
            :class:`cgp_maya_utils.scene.NurbsSurface`,
            :class:`cgp_maya_utils.scene.OrientConstraint`,
            :class:`cgp_maya_utils.scene.ParentConstraint`,
            :class:`cgp_maya_utils.scene.PointConstraint`,
            :class:`cgp_maya_utils.scene.Reference`,
            :class:`cgp_maya_utils.scene.ScaleConstraint`,
            :class:`cgp_maya_utils.scene.Shape`,
            :class:`cgp_maya_utils.scene.SkinCluster`,
            :class:`cgp_maya_utils.scene.Transform`
    """

    pass


def getNodes(namePattern=None, nodeTypes=None, asExactNodeTypes=False):
    """get the existing nodes in the scene

    :param namePattern: name pattern of the nodes to get - ex : *_suffix
    :type namePattern: str

    :param nodeTypes: types of nodes to get in the scene
    :type nodeTypes: list[str]

    :param asExactNodeTypes: ``True`` : list only exact node types - ``False`` : all types inheriting node types
    :type asExactNodeTypes: bool

    :return: the listed Nodes
    :rtype: tuple[:class:`cgp_maya_utils.scene.Node`,
                  :class:`cgp_maya_utils.scene.AimConstraint`,
                  :class:`cgp_maya_utils.scene.AnimCurve`,
                  :class:`cgp_maya_utils.scene.AnimCurveTA`,
                  :class:`cgp_maya_utils.scene.AnimCurveTL`,
                  :class:`cgp_maya_utils.scene.AnimCurveTU`,
                  :class:`cgp_maya_utils.scene.Constraint`,
                  :class:`cgp_maya_utils.scene.DagNode`,
                  :class:`cgp_maya_utils.scene.GeometryFilter`,
                  :class:`cgp_maya_utils.scene.IkEffector`,
                  :class:`cgp_maya_utils.scene.IkHandle`,
                  :class:`cgp_maya_utils.scene.Joint`,
                  :class:`cgp_maya_utils.scene.Mesh`,
                  :class:`cgp_maya_utils.scene.Node`,
                  :class:`cgp_maya_utils.scene.NurbsCurve`,
                  :class:`cgp_maya_utils.scene.NurbsSurface`,
                  :class:`cgp_maya_utils.scene.OrientConstraint`,
                  :class:`cgp_maya_utils.scene.ParentConstraint`,
                  :class:`cgp_maya_utils.scene.PointConstraint`,
                  :class:`cgp_maya_utils.scene.Reference`,
                  :class:`cgp_maya_utils.scene.ScaleConstraint`,
                  :class:`cgp_maya_utils.scene.Shape`,
                  :class:`cgp_maya_utils.scene.SkinCluster`,
                  :class:`cgp_maya_utils.scene.Transform`]
    """

    pass


def node(name):
    """the node object from a node name

    :param name: the name of the node
    :type name: str

    :return: the node object
    :rtype: :class:`cgp_maya_utils.scene.Node`,
            :class:`cgp_maya_utils.scene.AimConstraint`,
            :class:`cgp_maya_utils.scene.AnimCurve`,
            :class:`cgp_maya_utils.scene.AnimCurveTA`,
            :class:`cgp_maya_utils.scene.AnimCurveTL`,
            :class:`cgp_maya_utils.scene.AnimCurveTU`,
            :class:`cgp_maya_utils.scene.Constraint`,
            :class:`cgp_maya_utils.scene.DagNode`,
            :class:`cgp_maya_utils.scene.GeometryFilter`,
            :class:`cgp_maya_utils.scene.IkEffector`,
            :class:`cgp_maya_utils.scene.IkHandle`,
            :class:`cgp_maya_utils.scene.Joint`,
            :class:`cgp_maya_utils.scene.Mesh`,
            :class:`cgp_maya_utils.scene.Node`,
            :class:`cgp_maya_utils.scene.NurbsCurve`,
            :class:`cgp_maya_utils.scene.NurbsSurface`,
            :class:`cgp_maya_utils.scene.OrientConstraint`,
            :class:`cgp_maya_utils.scene.ParentConstraint`,
            :class:`cgp_maya_utils.scene.PointConstraint`,
            :class:`cgp_maya_utils.scene.Reference`,
            :class:`cgp_maya_utils.scene.ScaleConstraint`,
            :class:`cgp_maya_utils.scene.Shape`,
            :class:`cgp_maya_utils.scene.SkinCluster`,
            :class:`cgp_maya_utils.scene.Transform`
    """

    pass
