"""
maya decorators - also usable as contexts
"""

# imports third-parties
import cgp_generic_utils.decorators


class DisableAutoKey(cgp_generic_utils.decorators.Decorator):
    """decorator that disable animation automatic keying and set it back to its original state
    """

    def __init__(self):
        """DisableAutoKey class initialization
        """

        pass

    def __enter__(self):
        """enter DisableAutoKey decorator
        """

        pass

    def __exit__(self, *args, **kwargs):
        """exit DisableAutoKey decorator
        """

        pass


class DisableViewport(cgp_generic_utils.decorators.Decorator):
    """decorator that disable viewport drawing and reactivate it
    """

    def __init__(self):
        """DisableViewport class initialization
        """

        pass

    def __enter__(self):
        """enter DisableViewport decorator
        """

        pass

    def __exit__(self, *args, **kwargs):
        """exit DisableViewport decorator
        """

        pass


class KeepCurrentFrame(cgp_generic_utils.decorators.Decorator):
    """decorator that force the current frame to remain the same
    """

    def __init__(self):
        """KeepCurrentFrame class initialization
        """

        pass

    def __enter__(self):
        """enter KeepCurrentFrame decorator
        """

        pass

    def __exit__(self, *args, **kwargs):
        """exit KeepCurrentFrame decorator
        """

        pass


class KeepCurrentFrameRange(cgp_generic_utils.decorators.Decorator):
    """decorator that force the current frame range to remain the same
    """

    def __init__(self):
        """KeepCurrentFrameRange class initialization
        """

        pass

    def __enter__(self):
        """enter KeepCurrentFrameRange decorator
        """

        pass

    def __exit__(self, *args, **kwargs):
        """exit KeepCurrentFrameRange decorator
        """

        pass


class KeepCurrentSelection(cgp_generic_utils.decorators.Decorator):
    """decorator that force the current selection to remain the same
    """

    def __init__(self):
        """KeepCurrentSelection class initialization
        """

        pass

    def __enter__(self):
        """enter KeepCurrentSelection decorator
        """

        pass

    def __exit__(self, *args, **kwargs):
        """exit KeepCurrentSelection decorator
        """

        pass


class NamespaceContext(cgp_generic_utils.decorators.Decorator):
    """decorator that force the script to be executed in the context of a namespace
    and set the current namespace back to the previous one
    """

    def __init__(self, contextNamespace=':'):
        """NamespaceContext class initialization

        :param contextNamespace: context nameSpace the decorator will go in to execute the decorated command
        :type contextNamespace: str or :class:`cgp_maya_utils.scene.Namespace`
        """

        pass

    def __enter__(self):
        """enter NamespaceContext decorator
        """

        pass

    def __exit__(self, *args, **kwargs):
        """exit NamespaceContext decorator
        """

        pass


class UndoChunk(cgp_generic_utils.decorators.Decorator):
    """decorator that encapsulate the script into its own undo chunk
    """

    def __init__(self, name=None, isUndoing=True):
        """UndoChunk class initialization

        :param name: name of the undoChunk
        :type name: str

        :param name: ``True`` : Actions are undone if error occurred - ``False`` : Actions are not undone
        :type name: str
        """

        pass

    def __enter__(self):
        """enter UndoChunk decorator
        """

        pass

    def __exit__(self, *args, **kwargs):
        """exit UndoChunk decorator
        """

        pass
