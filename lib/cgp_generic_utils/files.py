"""
file objects and management functions
"""


class Path(object):
    """path object that manipulates any kind of entity on the file system
    """

    # INIT #

    def __init__(self, path):
        """Path class initialization

        :param path: path of the entity
        :type path: str
        """

        pass

    def __eq__(self, path):
        """check if the Path has the same name as the other path

        :param path: path to check with
        :type path: str or :class:`cgp_generic_utils.files.Path`

        :return : ``True`` : the paths are identical - ``False`` : the paths are different
        :rtype: bool
        """

        pass

    def __ne__(self, path):
        """check if the Path has not the same name as the other path

        :param path: path to check with
        :type path: str or :class:`cgp_generic_utils.files.Path`

        :return : ``True`` : the paths are different - ``False`` : the paths are identical
        :rtype: bool
        """

        pass

    def __repr__(self):
        """the representation of the path

        :return: the representation of the path
        :rtype: str
        """

        pass

    def __str__(self):
        """the print of the path

        :return: the print of the path
        :rtype: str
        """

        pass

    # COMMANDS #

    def baseName(self, withExtension=True):
        """the baseName of the path

        :param withExtension: ``True`` : baseName with extension - ``False`` : baseName without extension
        :type withExtension: bool

        :return: the baseName of the path
        :rtype: str
        """

        pass

    def directory(self):
        """the parent directory of the path

        :return: the directory of the path
        :rtype: :class:`cgp_generic_utils.files.Directory`
        """

        pass

    def extension(self):
        """the extension of the path

        :return: the extension of the path
        :rtype: str
        """

        pass

    def isDirectory(self):
        """check if the path is a directory

        :return: ``True`` : the path is a directory - ``False`` the path is not a directory
        :rtype: bool
        """

        pass

    def isFile(self):
        """check if the path is a file

        :return: ``True`` : the path is a file - ``False`` the path is not a file
        :rtype: bool
        """

        pass

    def path(self):
        """the path of the entity on the file system

        :return: the path of the entity
        :rtype: str
        """

        pass

    def pathType(self):
        """the type of the path

        :return: the type of the path
        :rtype: str
        """

        pass


class File(Path):
    """file object that manipulates any kind of file on the file system
    """

    # ATTRIBUTES #

    _extension = None

    # OBJECT COMMANDS #

    @classmethod
    def create(cls, path, content=None, **__):
        """create the file

        :param path: path of the file
        :type path: str

        :param content: content of the file
        :type content: any

        :return: the created file
        :rtype: :class:`cgp_generic_utils.files.File`
        """

        pass

    # COMMANDS #

    def copy(self, destinationDirectory=None, destinationName=None):
        """copy the file

        :param destinationDirectory: directory where the copied file will be saved  - If None, same as original
        :type destinationDirectory: str or :class:`cgp_generic_utils.files.Directory`

        :param destinationName: name of the copied file - If None, same as original - ! HAS TO BE WITHOUT EXTENSION !
        :type destinationName: str

        :return: the copied file
        :rtype: :class:`cgp_generic_utils.files.File`
        """

        pass

    def evaluate(self, asLiteral=True):
        """evaluate the content of the file

        :param asLiteral: ``True`` : content evaluated with ast.literal_eval - ``False`` : content evaluated with eval
        :type asLiteral: bool

        :return: the evaluated content of the file
        :rtype: any
        """

        pass

    def execute(self):
        """execute the content of the file

        :return: the executed content of the file
        :rtype: any
        """

        pass

    def open(self):
        """open the file in the script editor
        """

        pass

    def read(self):
        """read the file

        :return: the content of the file
        :rtype: any
        """

        pass

    def write(self, content):
        """write data in the specified path file

        :param content: content to write in the file
        :type content: any
        """

        pass


class Directory(Path):
    """directory object that manipulates a directory on the file system
    """

    # OBJECT COMMANDS #

    @classmethod
    def create(cls, path, **__):
        """create

        :param path: path of the file to create
        :type path: str

        :return: the created directory
        :rtype: :class:`cgp_generic_utils.files.Directory`
        """

        pass

    # COMMANDS #

    def baseName(self):
        """the baseName of the directory

        :return: the baseName of the directory
        :rtype: str
        """

        pass

    def content(self, fileFilters=None, fileExtensions=None, fileExtensionsIncluded=True):
        """content of the directory

        :param fileFilters: filter of the directory children - default is ``cgp_generic_utils.constants.FileFilter.ALL``
        :type fileFilters: list[str]

        :param fileExtensions: extensions of the files to get - default is all extensions
        :type fileExtensions: list[str]

        :param fileExtensionsIncluded: ``True`` : file extensions are included -
                                       ``False`` : file extensions are excluded
        :type fileExtensionsIncluded: bool

        :return: the content of the directory
        :rtype: list[:class:`cgp_generic_utils.files.Directory`,
                :class:`cgp_generic_utils.files.File`,
                :class:`cgp_generic_utils.files.JsonFile`,
                :class:`cgp_generic_utils.files.PyFile`,
                :class:`cgp_generic_utils.files.TxtFile`,
                :class:`cgp_generic_utils.files.UiFile`]
        """

        pass


class TxtFile(File):
    """file object that manipulates a ``.txt`` file on the file system
    """

    # ATTRIBUTES #

    _extension = 'txt'


class UiFile(File):
    """file object that manipulates a ``.ui`` file on the file system
    """

    # ATTRIBUTES #

    _extension = 'ui'

    # OBJECT COMMANDS #

    @classmethod
    def create(cls, path, content=None, **__):
        """create a ui file

        :param path: path of the ui file
        :type path: str

        :param content: content of the ui file
        :type content: any

        :return: the created ui file
        :rtype: :class:`cgp_generic_utils.files.UiFile`
        """

        pass

    # COMMANDS #

    def compile(self, targetDirectory=None):
        """compile the ui file

        :param targetDirectory: directory of the compiled Ui - if not specified, command will use the UiFile directory
        :type targetDirectory: str or :class:`cgp_generic_utils.files.Directory`

        :return: the compiled file
        :rtype: :class:`cgp_generic_utils.files.PyFile`
        """

        pass


class JsonFile(File):
    """file object that manipulate a ``.json`` file on the file system
    """

    # ATTRIBUTES #

    _extension = 'json'

    # OBJECT COMMANDS #

    @classmethod
    def create(cls, path, content=None, **__):
        """create a json file

        :param path: path of the json file
        :type path: str

        :param content: content of the json file
        :type content: any

        :return: the created json file
        :rtype: :class:`cgp_generic_utils.files.JsonFile`
        """

        pass

    # COMMANDS #

    def read(self):
        """read the json file

        :return: the content of the json file
        :rtype: any
        """

        pass


class PklFile(File):
    """file object that manipulate a ``.pkl`` file on the file system
    """

    # ATTRIBUTES #

    _extension = 'json'

    # OBJECT COMMANDS #

    @classmethod
    def create(cls, path, content=None, **__):
        """create a pkl file

        :param path: path of the pkl file
        :type path: str

        :param content: content of the pkl file
        :type content: any

        :return: the created pkl file
        :rtype: :class:`cgp_generic_utils.files.PklFile`
        """

        pass

    # COMMANDS #

    def read(self):
        """read the pkl file

        :return: the content of the pkl file
        :rtype: any
        """

        pass


class PyFile(File):
    """file object that manipulates a ``.py`` file on the file system
    """

    # ATTRIBUTES #

    _extension = 'py'

    # COMMANDS #

    def importAsModule(self):
        """import the python file as module

        :return: the module object
        :rtype: python
        """

        pass


def createDirectory(path):
    """create a directory on the file system

    :param path: path of the directory to create
    :type path: str

    :return: the created directory
    :rtype: :class:`cgp_generic_utils.files.Directory`
    """

    pass


def createFile(path, content=None, **extraData):
    """create a file on the file system

    :param path: path of the file to create
    :type path: str

    :param content: content to set into the created file
    :type content: any

    :param extraData: extra data used to create the file
    :type extraData: dict

    :return: the created file
    :rtype: :class:`cgp_generic_utils.files.File`
    """

    pass


def entity(path):
    """a file/directory object from a path

    :param path: path of the file/directory to get the entity from
    :type path: str

    :return: the file/directory
    :rtype: :class:`cgp_generic_utils.files.Directory`,
            :class:`cgp_generic_utils.files.File`,
            :class:`cgp_generic_utils.files.JsonFile`,
            :class:`cgp_generic_utils.files.PyFile`,
            :class:`cgp_generic_utils.files.TxtFile`,
            :class:`cgp_generic_utils.files.UiFile`,
    """

    pass


def registerFileTypes(fileTypes):
    """register file types to grant file management functions access to the file objects

    :param fileTypes: types of files to register - {extension1: FileObject1, extension2: FileObject2 ...}
    :type fileTypes: dict
    """

    pass
