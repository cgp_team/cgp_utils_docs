"""
python management functions
"""


def deleteModules(*args):
    """ delete modules that contain the arguments
    """

    pass


def import_(module, command=None):
    """import the module. return module.command if command is specified

    :param module: module of the command to import
    :type module: str

    :param command: command to import
    :type command: str

    :return: the imported function
    :rtype: function
    """

    pass
