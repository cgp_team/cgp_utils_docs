"""
generic python decorators - also usable as contexts
"""

# imports python
import functools


class Decorator(object):
    """decorator handling decoration and context behavior through subclass inheritance
    """

    def __call__(self, function):
        """override call

        :param function: function to decorate
        :type function: python

        :return: the function wrapper
        :rtype: python
        """

        # execute
        @functools.wraps(function)
        def functionWrapper(*args, **kwargs):
            """get the function wrapper

            :return: the result of the wrapped function
            :rtype: python
            """

            # execute
            with self:
                return function(*args, **kwargs)

        # return
        return functionWrapper


class Profiler(Decorator):
    """decorator returning the profiling of the encapsulated script block / function
    """

    def __init__(self, sortBy=None):
        """Profiler class initialization

        :param sortBy: defines how the stats will be sorted - default is time
        :type sortBy: str
        """

        pass

    def __enter__(self):
        """enter Profiler decorator
        """

        pass

    def __exit__(self, *args, **kwargs):
        """exit Profiler decorator
        """

        pass


class StatusDialog(Decorator):
    """decorator poping a status dialog
    """

    def __init__(self,
                 loadDescription,
                 validDescription,
                 errorDescription,
                 title,
                 size=None,
                 parent=None,
                 isFrameless=False):
        """StatusDialog class initialization

        :param loadDescription: description to set when the dialog is loaded
        :type loadDescription: str

        :param validDescription: description to set when the dialog closes with no occurring errors
        :type validDescription: str

        :param errorDescription: description to set when the dialog closes with errors
        :type errorDescription: str
        """
        pass

    def __enter__(self):
        """enter DisableAutokey decorator
        """

        pass

    def __exit__(self, *args, **kwargs):
        """exit DisableAutokey decorator
        """

        pass


class Timer(Decorator):
    """decorator returning the execution time of the encapsulated script block / function
    """

    def __init__(self, label=None):
        """Timer class initialization

        :param label: text that will be attached to the timer result
        :type label: str
        """

        pass

    def __enter__(self):
        """enter Timer decorator
        """

        pass

    def __exit__(self, *args, **kwargs):
        """exit Timer decorator
        """

        pass
