"""
subclassed QtWidgets, custom dialogs and widgets for UI development
"""

import PySide2.QtWidgets
import PySide2.QtGui
import PySide2.QtCore


class CollapsibleWidget(PySide2.QtWidgets.QWidget):
    """widget that allows to hide/show its content by collapsing/expanding
    """

    collapseChanged = 'PySide2.QtCore.Signal(bool)'

    # INIT #

    def __init__(self, title, titleHeight, backgroundColor=None, foregroundColor=None,
                 orientation=None, margins=None, spacing=0, isCollapsed=True, parent=None):
        """CollapsibleWidget class initialization

        :param title: title of the header
        :type title: str

        :param titleHeight: height of the header title
        :type titleHeight: int

        :param backgroundColor: background color of the header - [1, 1, 1] is default
        :type backgroundColor: list[int, float]

        :param foregroundColor: foreground color of the header - [1, 1, 1] is default
        :type foregroundColor: list[int, float]

        :param orientation: orientation of the content layout - [Orientation.HORIZONTAL - Orientation.VERTICAL]
        :type orientation: str

        :param margins: values of the margins of the content layout - [0, 0, 0, 0] is default
        :type margins: list[int]

        :param spacing: value of the spacing of the content layout
        :type spacing: int

        :param isCollapsed: defines whether or not the widget is collapsed at initialization
        :type isCollapsed: bool

        :param parent: widget under which the CollapsibleWidget will be parented
        :type parent: :class:`Pyside2.QtWidgets.QWidget`
        """

        pass

    # COMMANDS

    def addLayout(self, layout):
        """add layout to the CollapsibleWidget

        :param layout: layout to add to the CollapsibleWidget
        :type layout: :class:`PySide2.QtWidgets.QLayout`
        """

        pass

    def addWidget(self, widget):
        """add widget to the CollapsibleWidget

        :param widget: widget to add to the CollapsibleWidget
        :type widget: :class:`PySide2.QtWidgets.QWidget`
        """

        pass

    def collapse(self):
        """collapse the content of the CollapsibleWidget
        """

        pass

    def contentWidget(self):
        """get the content widget

        :return: the content widget
        :rtype: :class:`PySide2.QtWidgets.QWidget`
        """

        pass

    def expand(self):
        """expand the content of the CollapsibleWidget
        """

        pass

    def isCollapsed(self):
        """get the collapsed state of the CollapsibleWidget

        :return: the collapsed state
        :rtype: bool
        """

        pass

    def toggleCollapse(self):
        """toggle collapse state of the CollapsibleWidget
        """

        pass


class Tool(PySide2.QtWidgets.QWidget):
    """widget handling UI tool generic functionalities through subclass inheritance
    """

    # ATTRIBUTES #

    _object = None
    _name = NotImplemented

    # INIT #

    def __init__(self, parent=None):
        """Tool class initialization

        :param parent: parent under which the ui will be parented
        :type parent: :class:`PySide2.QtWidgets.QWidget`
        """

        pass

    def __new__(cls, *args, **kwargs):
        """override new method

        :return: the created instance
        :rtype: :class:`cgp_generic_utils.qt.Tool`
        """

        pass

    # OBJECT COMMANDS #

    @classmethod
    def load(cls, *args, **kwargs):
        """load the tool with the arguments and keywords of the init
        """

        pass

    # COMMANDS #

    def name(self):
        """get the name of the tool

        :return: the name of the tool
        :rtype: str
        """

        pass

    def setTitle(self, title=None):
        """set the title of the widget

        :param title: title of the widget to set - if nothing specified title will be -> name version
        :type title: str
        """

        pass


class BaseDialog(PySide2.QtWidgets.QDialog):
    """basic dialog with ok and cancel button
    """

    # INIT #

    def __init__(self, title, label, size=None, parent=None, isFrameless=False, orientation=None):
        """BaseDialog class initialization

        :param title: the title of the dialog
        :type title: str

        :param label: the content of the label to set
        :type label: str

        :param size: values of the width and the height  ``[width - height]`` - default is ``[None, None]``
        :type size: list[int]

        :param parent: QWidget to parent the TextEdit dialog to
        :type parent: :class:`PySide2.QtWidgets.QWidget`

        :param isFrameless: ``True`` : dialog is frameless - ``False`` : dialog has a frame
        :type isFrameless: bool

        :param orientation: orientation of the content layout -
                            default is ``cgp_generic_utils.constants.Orientation.VERTICAL``
        :type orientation: str
        """

        pass

    def __setupConnections(self):
        """setup the connections of the widgets
        """

        pass

    # COMMANDS #

    def contentLayout(self):
        """the content layout

        :return: the content layout
        :rtype: :class:`PySide2.QtWidgets.QBoxLayout`
        """

        pass

    def isValid(self):
        """check if the dialog is valid

        :return: ``True`` it is valid - ``False`` it is not valid
        :rtype: bool
        """

        pass

    def load(self):
        """load base dialog

        :return: ``cancel`` : self.rejection - ``ok`` : self.validation
        :rtype: any
        """

        pass

    def rejection(self):
        """result of the dialog when ``cancel`` is pressed

        :return: the rejection of the dialog
        :rtype: any
        """

        # execute
        return None

    def setCancelButton(self, text=None):
        """set the text of the cancel button

        :param text: text to set on the cancel button
        :type text: str
        """

        pass

    def setOkButton(self, text=None):
        """set the text ok the ok button

        :param text: text to set on the ok button
        :type text: str
        """

        pass

    def setStatus(self, okEnabled=True):
        """set the status of the ok button

        :param okEnabled: ``True`` : the ok button is enabled - ``False`` the ok button is disabled
        :type okEnabled: bool
        """

        pass

    def setSize(self, size):
        """set the size of the dialog

        :param size: values of the width and the height  ``[width - height]``
        :type size: list[int]
        """

        pass

    def validation(self):
        """result of the dialog when ``ok`` is pressed

        :return: the validation of the dialog
        :rtype: any
        """

        # execute
        return True


class CheckBoxDialog(BaseDialog):
    """dialog with checkbox checking
    """

    # INIT #

    def __init__(self, title, label, data, size=None, parent=None, isFrameless=False):
        """CheckBoxDialog class initialization

        :param title: the title of the dialog
        :type title: str

        :param label: the content of the label to set
        :type label: str

        :param data: data used to labels and states to populate the checkBoxes - ``{label1: True, label2: False}``
        :type data: dict

        :param size: values of the width and the height  ``[width - height]`` - default is ``[None, None]``
        :type size: list[int]

        :param parent: QWidget to parent the line edit dialog to
        :type parent: :class:`PySide2.QtWidgets.QWidget`

        :param isFrameless: ``True`` : dialog is frameless - ``False`` : dialog has a frame
        :type isFrameless: bool
        """

        pass

    # COMMANDS #

    def addCheckbox(self, label, isChecked):
        """add a new check box to the dialog

        :param label: label of the checkbox
        :type label: str

        :param isChecked: ``True`` checkbox is checked - ``False`` checkbox is not checked
        :type isChecked: bool

        :return: the added checkbox
        :rtype: :class:`PySide2.QtWidgets.QCheckBox`
        """

        pass

    # PRIVATE COMMANDS #

    def validation(self):
        """validation of the dialog

        :return: ``Cancel`` : None - ``Ok`` : {checkBoxLabel1: isChecked1, checkBoxLabel2: isChecked2 ...}
        :rtype: None or dict
        """

        pass


class ComboBoxDialog(BaseDialog):
    """dialog with combobox selection
    """

    # INIT #

    def __init__(self, title, label, data, entryLabelWidth=35, size=None,
                 parent=None, isFrameless=False, orientation=None):
        """ComboBoxDialog class initialization

        :param title: the title of the dialog
        :type title: str

        :param label: the content of the label to set
        :type label: str

        :param data: data used to build the comboBoxes - ``[[label1, [itemA, itemB ...]], [label2, [...]], ...]``
        :type data: list[list[str, list[str]]]

        :param entryLabelWidth: with of the label of the combobox entries
        :type entryLabelWidth: int

        :param size: values of the width and the height  ``[width - height]`` - default is ``[None, None]``
        :type size: list[int]

        :param parent: QWidget to parent the line edit dialog to
        :type parent: :class:`PySide2.QtWidgets.QWidget`

        :param isFrameless: ``True`` : dialog is frameless - ``False`` : dialog has a frame
        :type isFrameless: bool

        :param orientation: defines the orientation of the content layout -
                            default is ``cgp_generic_utils.constants.Orientation.VERTICAL``
        :type orientation: :class:`cgp_generic_utils.constants.Orientation`
        """

        pass

    # COMMAND #

    def addCombobox(self, label, items):
        """add a new check box to the dialog

        :param label: label of the combobox
        :type label: str

        :param items: items of the combobox
        :type items: list[str]
        """

        pass

    # PRIVATE COMMANDS #

    def validation(self):
        """validation of the dialog

        :return: ``Cancel`` : None - ``Ok`` : list of combobox currentTexts
        :rtype: None or list[str]
        """

        pass


class ComboBoxLineEditDialog(BaseDialog):
    """dialog with combobox selection and lineEdit filling
    """

    # INIT #

    def __init__(self, title, label, items, icons=None, iconSize=None, text=None, comboSeparator=None,
                 separator=None, concatenateResult=True, size=None, parent=None, isFrameless=False):
        """ComboBoxLineEditDialog class initialization

        :param title: the title of the dialog
        :type title: str

        :param label: the content of the label to set
        :type label: str

        :param icons: icons used to populate the combobox
        :type icons: list[str]

        :param iconSize: size of the icons - default is ``[10, 10]``
        :type iconSize: list[int]

        :param items: item used to populate the combobox
        :type items: list[str]

        :param text: the text that will be displayed in the lineEdit at initialization
        :type text: str

        :param comboSeparator: separator used to split combobox content if specified
        :type comboSeparator: str

        :param separator: separator used when merging current combobox item with lineEdit text
        :type separator: str

        :param concatenateResult: defines whether or not the result will be concatenated between combobox and lineEdit
        :type concatenateResult: bool

        :param size: values of the width and the height  ``[width - height]`` - default is ``[None, None]``
        :type size: list[int]

        :param parent: QWidget to parent the line edit dialog to
        :type parent: :class:`PySide2.QtWidgets.QWidget`

        :param isFrameless: ``True`` : dialog is frameless - ``False`` : dialog has a frame
        :type isFrameless: bool
        """

        pass

    def _setupConnections(self):
        """setup the connections of the widgets
        """

        pass

    # COMMANDS #

    def setIcons(self, icons):
        """set the items icons of the combobox

        :param icons: icons used to set the combobox
        :type icons: list[str]
        """

        pass

    def setItems(self, items=None):
        """set the items of the combobox

        :param items: items used to set the combobox
        :type items: list[str]
        """

        pass

    def setText(self, text=None):
        """set the text of the line edit

        :param text: text used to set the line edit
        :type text: str
        """

        pass

    # PRIVATE COMMANDS #

    def _setOkStatus(self):
        """set the status of the okButton
        """

        pass

    def validation(self):
        """validation of the dialog

        :return: ``Cancel`` : None -
                 ``Ok`` : if comboSeparator ``[{combobox.split(comboSeparator)[0]}{separator}{lineEdit} ...]``
                 else ``{combobox}{separator}{lineEdit}``
        :rtype: None or list[str] or str
        """

        pass


class LineEditDialog(BaseDialog):
    """dialog with lineEdit filling
    """

    # INIT #

    def __init__(self, title, label, data=None, entryLabelWidth=35,
                 size=None, parent=None, isFrameless=False):
        """LineEditDialog class initialization

        :param title: the title of the dialog
        :type title: str

        :param label: the content of the label to set
        :type label: str

        :param data: data used to create the entry lineEdits - ``[[label1, text1], ...]`` -
                     default is ``[[None, None]]``
        :type data: list[list[str]]

        :param entryLabelWidth: with of the label of the lineEdit entries
        :type entryLabelWidth: int

        :param size: values of the width and the height ``[width - height]`` - default is ``[None, None]``
        :type size: list[int]

        :param parent: QWidget to parent the line edit dialog to
        :type parent: :class:`PySide2.QtWidgets.QWidget`

        :param isFrameless: ``True`` : dialog is frameless - ``False`` : dialog has a frame
        :type isFrameless: bool
        """

        pass

    def _setupConnections(self):
        """setup the connections of the widgets
        """

        pass

    # COMMANDS #

    def addLineEdit(self, label, text):
        """add a new lineEdit to the dialog

        :param label: label of the lineEdit
        :type label: str

        :param text: text of the lineEdit
        :type text: str
        """

        pass

    # PRIVATE COMMANDS #

    def _setOkStatus(self):
        """set the status of the okButton
        """

        pass

    def validation(self):
        """validation of the dialog

        :return: ``Cancel`` : None - ``Ok`` : lineEdit contents
        :rtype: None or list[str]
        """

        pass


class StatusDialog(BaseDialog):
    """dialog displaying current status
    """

    # INIT #

    def __init__(self, title, size=None, parent=None, isFrameless=False):
        """StatusDialog class initialization

        :param title: the title of the dialog
        :type title: str

        :param size: values of the width and the height ``[width - height]`` - default is ``[None, None]``
        :type size: list[int]

        :param parent: QWidget to parent the line edit dialog to
        :type parent: :class:`PySide2.QtWidgets.QWidget`

        :param isFrameless: ``True`` : dialog is frameless - ``False`` : dialog has a frame
        :type isFrameless: bool
        """

        # init
        pass

    def _setupConnections(self):
        """setup connections
        """

        # execute
        pass

    # COMMANDS #

    def close(self, description, closeTime=1):
        """close status dialog

        :param description: description to set before the dialog closes
        :type description: str

        :param closeTime: time in seconds before the dialog closes
        :type closeTime: float
        """

        pass

    def load(self, description):
        """load status dialog

        :param description: description to set when the dialog is loaded
        :type description: str
        """

        pass


class TextEditDialog(BaseDialog):
    """dialog with textEdit filling
    """

    # INIT #

    def __init__(self, title, label, text=None, size=None, parent=None, isFrameless=False):
        """TextEditDialog class initialization

        :param title: the title of the dialog
        :type title: str

        :param label: the content of the label to set
        :type label: str

        :param text: the text that will be displayed in the TextEdit at initialization
        :type text: str

        :param size: values of the width and the height ``[width - height]`` - default is ``[None, None]``
        :type size: list[int]

        :param parent: QWidget to parent the TextEdit dialog to
        :type parent: :class:`PySide2.QtWidgets.QWidget`

        :param isFrameless: ``True`` : dialog is frameless - ``False`` : dialog has a frame
        :type isFrameless: bool
        """

        pass

    def _setupConnections(self):
        """setup the connections of the widgets
        """

        pass

    # COMMANDS #

    def setText(self, text=None):
        """set the text of the line edit

        :param text: text used to set the line edit
        :type text: str
        """

        pass

    # PRIVATE COMMANDS #

    def _setOkStatus(self):
        """set the status of the okButton
        """

        pass

    def validation(self):
        """validation of the dialog

        :return: ``Cancel`` : None - ``Ok`` : textEdit content
        :rtype: None or str
        """

        pass


class Font(PySide2.QtGui.QFont):
    """QFont with custom functionalities
    """

    def __init__(self, size, styles=None):
        """Font class initialization

        :param size: size of the font
        :type size: int

        :param styles: styles of the font - [bold - italic - underline]
        :type: list[:class:`cgp_generic_utils.constants.TypoStyle`]
        """

        pass


class Icon(PySide2.QtGui.QIcon):
    """QIcon with custom functionalities
    """

    # INIT #

    def __init__(self, image):
        """Icon class initialization
        """

        pass

    # COMMANDS #

    def path(self):
        """the path of the image used by the icon

        :return: the path of the image
        :rtype: str
        """

        pass


class ComboBox(PySide2.QtWidgets.QComboBox):
    """QComboBox with custom functionalities
    """

    # COMMANDS #

    def goTo(self, text):
        """go to the item with the specified text

        :param text: text used to select the proper item
        :type text: str
        """

        pass


class LineEdit(PySide2.QtWidgets.QLineEdit):
    """QLineEdit with custom functionalities
    """

    # COMMANDS #

    def setBackground(self, color=None, pattern='solid'):
        """set the background color of the lineEdit using the specified color and pattern

        :param color: color to set - [R, V, B, A] - [0.8, 0.8, 0.8, 1] is default
        :type color: list[int, float]

        :param pattern: pattern of the background
        :type pattern: str
        """

        pass

    def setForeground(self, color=None):
        """set the foreground color of the lineEdit using the specified color and pattern

        :param color: color to set - [R, V, B, A] - [0.8, 0.8, 0.8, 1] is default
        :type color: list[int, float]
        """

        pass

    def setFrameColor(self, color=None):
        """set the frame color of the lineEdit using the specified color

        :param color: color to set - [R, V, B, A] - [0.8, 0.8, 0.8, 1] is default
        :type color: list[int, float]
        """

        pass


class ListWidget(PySide2.QtWidgets.QListWidget):
    """QListWidget with custom functionalities
    """

    # COMMANDS #

    def addWidget(self, widget):
        """add widget to the QListWidget

        :param widget: widget to add to the QListWidget
        :type widget: :class:`PySide2.QtWidgets.QWidget`
        """

        pass

    def childrenItems(self):
        """get the children items of the list

        :return: the list of children items
        :rtype: list[:class:`PySide2.QtWidgets.QListWidgetItem`]
        """

        pass


class PushButton(PySide2.QtWidgets.QPushButton):
    """QPushButton with custom functionalities
    """

    # COMMANDS #

    def setIcon(self, image):
        """set the icon on the QPushButton

        :param image: image to set on the QPushButton
        :type image: str
        """

        pass

    def setIconSize(self, width, height):
        """set the icon size

        :param width: width of the icon
        :type width: int

        :param height: height of the icon
        :type height: int
        """

        pass


class TreeWidget(PySide2.QtWidgets.QTreeWidget):
    """QTreeWidget with custom functionalities
    """

    # COMMANDS #

    def childrenItems(self, recursive=False):
        """get the children items parented to the TreeWidget

        :param recursive: defines whether or not the command will get the children items recursively through hierarchy
        :type recursive: bool

        :return: list of children items
        :rtype: list[:class:`PySide2.QtWidgets.QTreeWidgetItem`, :class:`cgp_generic_utils.qt.TreeWidgetItem`]
        """

        pass

    def clearSelection(self):
        """clear the selection of the QTreeWidgetItem
        """

        pass

    def resizeToContent(self, margin=0):
        """resize the list of specified tre widgets to content

        :param margin: margin within the columns
        :type margin: int
        """

        pass


class TreeWidgetItem(PySide2.QtWidgets.QTreeWidgetItem):
    """QTreeWidgetItem with custom functionalities
    """

    # INIT #

    def __init__(self, parent=None):
        """QTreeWidgetItem class initialization

        :param parent: QWidget under which the QTreeWidgetItem will be parented
        :type parent: :class:`PySide2.QtWidgets.QTreeWidget` or :class:`PySide2.QtWidgets.QTreeWidgetItem`
        """

        pass

    # COMMANDS #

    def addWidget(self, widget, index):
        """add the specified widget to the specified column of the QTreeWidgetItem

        :param widget: widget to add the the QTreeWidgetItem
        :type widget: :class:`PySide2.QtWidgets.QWidget`

        :param index: index of the column where to add the widget
        :type index: int
        """

        pass

    def childrenItems(self, recursive=False):
        """get the children items parented to the QTreeWidgetItem

        :param recursive: defines whether or not the command will get the children items recursively through hierarchy
        :type recursive: bool

        :return: list of children
        :rtype: list[:class:`PySide2.QtWidgets.QTreeWidgetItem`, :class:`cgp_generic_utils.qt.TreeWidgetItem`]
        """

        pass

    def setBackgrounds(self, indexes, color=None, pattern='solid'):
        """set background colors of the columns of the specified indexes using the specified color and pattern

        :param indexes: list of the column indexes use to set the backgrounds
        :type indexes: list[int]

        :param color: color to set - [R, V, B, A] - [0.8, 0.8, 0.8, 1] is default
        :type color: list[int, float]

        :param pattern: pattern of the backgrounds
        :type pattern: str
        """

        pass

    def setFonts(self, indexes, size=9, styles=None):
        """set fonts of the columns of the specified indexes using the specified size and styles

        :param indexes: list of the column indexes use to set the fonts
        :type indexes: list[int]

        :param size: size of the font
        :type size: int

        :param styles: styles of the font - [bold - italic - underline]
        :type: list[:class:`cgp_generic_utils.constants.TypoStyle`]
        """

        pass

    def setIcons(self, indexes, icon):
        """set the icon of the specified column of the custom QTreeWidgetItem

        :param indexes: index of the column to set the icon to
        :type indexes: list

        :param icon: name of the icon to set
        :type icon: str
        """

        pass

    def setForegrounds(self, indexes, color=None, pattern='solid'):
        """set foreground colors of the columns of the specified indexes using the specified color and pattern

        :param indexes: list of the column indexes use to set the foregrounds
        :type indexes: list[int]

        :param color: color to set - [R, V, B, A] - [0.8, 0.8, 0.8, 1] is default
        :type color: list[int, float]

        :param pattern: pattern of the foregrounds
        :type pattern: str
        """

        pass

    def setSelectable(self, isSelectable):
        """set the status flags of the item

        :param isSelectable: defines whether or not the item is selectable
        :type isSelectable: bool
        """

        pass

    def setTexts(self, texts):
        """set texts in the specified column using the specified texts

        :param texts: dictionary holding column indexes and texts to set - {0: 'text1', 2:'text2' ...}
        :type texts: dict[int: str]
        """

        pass
