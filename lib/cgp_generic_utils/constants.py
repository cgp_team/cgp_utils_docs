"""
constants used to manipulate generic data through any DCC
"""

# imports python
import os
import collections


class Axis(object):

    X = 'x'
    Y = 'y'
    Z = 'z'
    ALL = [X, Y, Z]


class AxisTable(object):

    ALL = {'xy': 'z', 'yz': 'x', 'xz': 'y'}


class FileExtension(object):

    JSON = 'json'
    MA = 'ma'
    MB = 'mb'
    OBJ = 'obj'
    PY = 'py'
    TXT = 'txt'
    UI = 'ui'
    ALL = [JSON, MA, MB, OBJ, PY, TXT, UI]


class FileFilter(object):

    FILE = 'file'
    DIRECTORY = 'directory'
    ALL = [FILE, DIRECTORY]


class PathType(object):

    RELATIVE = 'relative'
    ABSOLUTE = 'absolute'
    ALL = [RELATIVE, ABSOLUTE]


class MirrorMode(object):

    NO_MIRROR = 'noMirror'
    MIRROR = 'mirror'
    NEG_MIRROR = 'negMirror'
    ALL = [NO_MIRROR, MIRROR, NEG_MIRROR]


class MirrorPlane(object):

    XY = 'xy'
    YZ = 'yz'
    XZ = 'xz'
    ALL = [XY, YZ, XZ]


class Environment(object):

    GENERIC_UTILS_ROOT = os.sep.join(os.path.dirname(__file__).split(os.sep)[:-3])
    ICON_LIBRARY = os.path.join(GENERIC_UTILS_ROOT, 'icons')
    SCRIPT_EDITOR = 'C:\\Program Files (x86)\\Notepad++\\notepad++.exe'


class LogType(object):

    PRINT = 'print'
    INFO = 'info'
    WARNING = 'warning'
    ERROR = 'error'
    ALL = [PRINT, INFO, WARNING, ERROR]


class Orientation(object):

    HORIZONTAL = 'horizontal'
    VERTICAL = 'vertical'
    ALL = [HORIZONTAL, VERTICAL]


class TransformMode(object):

    RELATIVE = 'relative'
    ABSOLUTE = 'absolute'
    ALL = [RELATIVE, ABSOLUTE]


class Side(object):

    LEFT = 'L'
    RIGHT = 'R'
    MIDDLE = 'M'
    MIRROR = collections.OrderedDict([(LEFT, RIGHT), (MIDDLE, None), (RIGHT, LEFT)])
    ALL = [LEFT, MIDDLE, RIGHT]


class TypoStyle(object):

    BOLD = 'bold'
    ITALIC = 'italic'
    UNDERLINE = 'underline'
    ALL = [BOLD, ITALIC, UNDERLINE]
