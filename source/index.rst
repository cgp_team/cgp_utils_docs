.. cgp_maya_utils documentation master file, created by
   sphinx-quickstart on Tue Nov 17 22:24:34 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

============================
CGP utils for Maya & Houdini
============================

python API for Maya and Houdini with generic utils facilitating other DCC utilities implementation

++++++++
Chapters
++++++++

.. toctree::
   :maxdepth: 1

   legal_information
   what_is_it
   installation
   get_started

++++++++++
Appendices
++++++++++

.. toctree::
   :maxdepth: 1

   modules
