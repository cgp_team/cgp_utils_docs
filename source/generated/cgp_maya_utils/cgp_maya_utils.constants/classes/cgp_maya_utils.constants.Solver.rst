Solver
===============================

.. currentmodule::  cgp_maya_utils.constants

.. autoclass:: Solver
    :members:
    :undoc-members: