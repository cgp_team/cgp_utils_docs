Transform
==================================

.. currentmodule::  cgp_maya_utils.constants

.. autoclass:: Transform
    :members:
    :undoc-members: