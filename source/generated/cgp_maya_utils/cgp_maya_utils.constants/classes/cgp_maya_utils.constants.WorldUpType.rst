WorldUpType
====================================

.. currentmodule::  cgp_maya_utils.constants

.. autoclass:: WorldUpType
    :members:
    :undoc-members: