SurfaceAssociation
===========================================

.. currentmodule::  cgp_maya_utils.constants

.. autoclass:: SurfaceAssociation
    :members:
    :undoc-members: