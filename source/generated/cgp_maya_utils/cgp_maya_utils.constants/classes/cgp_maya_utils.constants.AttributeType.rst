AttributeType
======================================

.. currentmodule::  cgp_maya_utils.constants

.. autoclass:: AttributeType
    :members:
    :undoc-members: