GeometryData
=====================================

.. currentmodule::  cgp_maya_utils.constants

.. autoclass:: GeometryData
    :members:
    :undoc-members: