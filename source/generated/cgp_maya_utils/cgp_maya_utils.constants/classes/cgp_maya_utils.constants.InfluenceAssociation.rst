InfluenceAssociation
=============================================

.. currentmodule::  cgp_maya_utils.constants

.. autoclass:: InfluenceAssociation
    :members:
    :undoc-members: