Environment
====================================

.. currentmodule::  cgp_maya_utils.constants

.. autoclass:: Environment
    :members:
    :undoc-members: