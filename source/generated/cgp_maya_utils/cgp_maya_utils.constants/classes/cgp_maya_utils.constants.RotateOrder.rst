RotateOrder
====================================

.. currentmodule::  cgp_maya_utils.constants

.. autoclass:: RotateOrder
    :members:
    :undoc-members: