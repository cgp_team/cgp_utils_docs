NodeType
=================================

.. currentmodule::  cgp_maya_utils.constants

.. autoclass:: NodeType
    :members:
    :undoc-members: