ComponentType
======================================

.. currentmodule::  cgp_maya_utils.constants

.. autoclass:: ComponentType
    :members:
    :undoc-members: