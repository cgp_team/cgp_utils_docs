TangentType
====================================

.. currentmodule::  cgp_maya_utils.constants

.. autoclass:: TangentType
    :members:
    :undoc-members: