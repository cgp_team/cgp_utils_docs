cgp\_maya\_utils.decorators
===========================

.. automodule:: cgp_maya_utils.decorators

    .. rubric:: Classes

    .. autosummary::
        :toctree: cgp_maya_utils.decorators/classes

            DisableAutoKey
            DisableViewport
            KeepCurrentFrame
            KeepCurrentFrameRange
            KeepCurrentSelection
            NamespaceContext
            UndoChunk
