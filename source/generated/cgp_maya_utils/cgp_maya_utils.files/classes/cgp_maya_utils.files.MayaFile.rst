MayaFile
=============================

.. currentmodule::  cgp_maya_utils.files

.. autoclass:: MayaFile
    :members:
    :undoc-members:
    :show-inheritance: