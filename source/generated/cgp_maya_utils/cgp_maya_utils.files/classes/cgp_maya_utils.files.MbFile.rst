MbFile
===========================

.. currentmodule::  cgp_maya_utils.files

.. autoclass:: MbFile
    :members:
    :undoc-members:
    :show-inheritance:
