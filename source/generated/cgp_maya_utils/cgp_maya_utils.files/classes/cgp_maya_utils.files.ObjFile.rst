ObjFile
============================

.. currentmodule::  cgp_maya_utils.files

.. autoclass:: ObjFile
    :members:
    :undoc-members:
    :show-inheritance: