MaFile
===========================

.. currentmodule::  cgp_maya_utils.files

.. autoclass:: MaFile
    :members:
    :undoc-members:
    :show-inheritance:
