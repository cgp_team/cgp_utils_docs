registerFileTypes
======================================

.. currentmodule:: cgp_maya_utils.files

.. autofunction:: registerFileTypes
