cgp\_maya\_utils.files
======================

.. automodule:: cgp_maya_utils.files

    .. rubric:: Classes

    .. autosummary::
        :toctree: cgp_maya_utils.files/classes

            MaFile
            MayaFile
            MbFile
            ObjFile


    .. rubric:: Functions

    .. autosummary::
        :toctree: cgp_maya_utils.files/functions

            registerFileTypes
