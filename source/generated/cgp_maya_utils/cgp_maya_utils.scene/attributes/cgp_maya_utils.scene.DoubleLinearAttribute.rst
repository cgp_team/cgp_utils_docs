DoubleLinearAttribute
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: DoubleLinearAttribute
    :members:
    :undoc-members:
    :show-inheritance: