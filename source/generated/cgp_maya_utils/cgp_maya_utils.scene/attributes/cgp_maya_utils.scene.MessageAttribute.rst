MessageAttribute
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: MessageAttribute
    :members:
    :undoc-members:
    :show-inheritance: