Attribute
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: Attribute
    :members:
    :undoc-members:
    :show-inheritance: