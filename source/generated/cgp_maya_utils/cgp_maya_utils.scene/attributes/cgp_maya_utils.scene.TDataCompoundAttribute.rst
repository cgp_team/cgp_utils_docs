TDataCompoundAttribute
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: TDataCompoundAttribute
    :members:
    :undoc-members:
    :show-inheritance: