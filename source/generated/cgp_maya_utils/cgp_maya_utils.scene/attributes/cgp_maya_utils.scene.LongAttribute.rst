LongAttribute
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: LongAttribute
    :members:
    :undoc-members:
    :show-inheritance: