DoubleAngleAttribute
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: DoubleAngleAttribute
    :members:
    :undoc-members:
    :show-inheritance: