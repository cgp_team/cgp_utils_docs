ShortAttribute
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: ShortAttribute
    :members:
    :undoc-members:
    :show-inheritance: