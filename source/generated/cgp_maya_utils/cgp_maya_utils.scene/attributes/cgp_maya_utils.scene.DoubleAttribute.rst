DoubleAttribute
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: DoubleAttribute
    :members:
    :undoc-members:
    :show-inheritance: