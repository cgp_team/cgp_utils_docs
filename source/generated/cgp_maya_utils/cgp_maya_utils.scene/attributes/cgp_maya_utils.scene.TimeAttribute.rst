TimeAttribute
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: TimeAttribute
    :members:
    :undoc-members:
    :show-inheritance: