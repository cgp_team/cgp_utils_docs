Connection
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: Connection
    :members:
    :undoc-members:
    :show-inheritance: