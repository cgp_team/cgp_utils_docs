StringAttribute
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: StringAttribute
    :members:
    :undoc-members:
    :show-inheritance: