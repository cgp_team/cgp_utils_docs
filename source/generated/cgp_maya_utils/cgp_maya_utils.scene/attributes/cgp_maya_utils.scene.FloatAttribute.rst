FloatAttribute
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: FloatAttribute
    :members:
    :undoc-members:
    :show-inheritance: