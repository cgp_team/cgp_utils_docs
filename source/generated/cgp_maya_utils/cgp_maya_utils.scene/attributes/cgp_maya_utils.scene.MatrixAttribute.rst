MatrixAttribute
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: MatrixAttribute
    :members:
    :undoc-members:
    :show-inheritance: