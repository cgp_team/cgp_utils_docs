BoolAttribute
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: BoolAttribute
    :members:
    :undoc-members:
    :show-inheritance: