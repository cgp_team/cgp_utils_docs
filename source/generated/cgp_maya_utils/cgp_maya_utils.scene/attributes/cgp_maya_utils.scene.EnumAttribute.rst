EnumAttribute
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: EnumAttribute
    :members:
    :undoc-members:
    :show-inheritance: