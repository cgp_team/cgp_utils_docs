ByteAttribute
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: ByteAttribute
    :members:
    :undoc-members:
    :show-inheritance: