Double3Attribute
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: Double3Attribute
    :members:
    :undoc-members:
    :show-inheritance: