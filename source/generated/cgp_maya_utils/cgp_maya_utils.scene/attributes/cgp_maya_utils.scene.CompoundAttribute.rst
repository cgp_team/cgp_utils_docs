CompoundAttribute
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: CompoundAttribute
    :members:
    :undoc-members:
    :show-inheritance: