NumericAttribute
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: NumericAttribute
    :members:
    :undoc-members:
    :show-inheritance: