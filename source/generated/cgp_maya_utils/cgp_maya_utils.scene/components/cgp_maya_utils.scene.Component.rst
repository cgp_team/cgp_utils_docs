Component
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: Component
    :members:
    :undoc-members:
    :show-inheritance: