Edge
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: Edge
    :members:
    :undoc-members:
    :show-inheritance: