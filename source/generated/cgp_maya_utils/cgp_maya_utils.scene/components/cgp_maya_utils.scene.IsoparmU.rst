IsoparmU
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: IsoparmU
    :members:
    :undoc-members:
    :show-inheritance: