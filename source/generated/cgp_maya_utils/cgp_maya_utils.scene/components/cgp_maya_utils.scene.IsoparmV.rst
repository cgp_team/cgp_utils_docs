IsoparmV
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: IsoparmV
    :members:
    :undoc-members:
    :show-inheritance: