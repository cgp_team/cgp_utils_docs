SurfacePoint
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: SurfacePoint
    :members:
    :undoc-members:
    :show-inheritance: