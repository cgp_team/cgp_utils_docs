Face
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: Face
    :members:
    :undoc-members:
    :show-inheritance: