SurfacePatch
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: SurfacePatch
    :members:
    :undoc-members:
    :show-inheritance: