CurvePoint
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: CurvePoint
    :members:
    :undoc-members:
    :show-inheritance: