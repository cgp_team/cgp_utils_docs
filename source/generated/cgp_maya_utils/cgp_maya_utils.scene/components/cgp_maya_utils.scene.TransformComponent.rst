TransformComponent
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: TransformComponent
    :members:
    :undoc-members:
    :show-inheritance: