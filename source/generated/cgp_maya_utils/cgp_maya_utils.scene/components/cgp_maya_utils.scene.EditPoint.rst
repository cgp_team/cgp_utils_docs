EditPoint
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: EditPoint
    :members:
    :undoc-members:
    :show-inheritance: