Vertex
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: Vertex
    :members:
    :undoc-members:
    :show-inheritance: