getAttributes
==================================

.. currentmodule:: cgp_maya_utils.scene

.. autofunction:: getAttributes
