getNodes
=============================

.. currentmodule:: cgp_maya_utils.scene

.. autofunction:: getNodes
