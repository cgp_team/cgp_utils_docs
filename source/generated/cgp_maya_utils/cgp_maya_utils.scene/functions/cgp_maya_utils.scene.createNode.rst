createNode
===============================

.. currentmodule:: cgp_maya_utils.scene

.. autofunction:: createNode
