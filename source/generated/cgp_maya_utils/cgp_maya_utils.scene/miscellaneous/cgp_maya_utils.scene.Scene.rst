Scene
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: Scene
    :members:
    :undoc-members:
    :show-inheritance: