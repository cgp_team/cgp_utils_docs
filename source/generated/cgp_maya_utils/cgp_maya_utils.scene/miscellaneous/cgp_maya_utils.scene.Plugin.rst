Plugin
===========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: Plugin
    :members:
    :undoc-members:
    :show-inheritance: