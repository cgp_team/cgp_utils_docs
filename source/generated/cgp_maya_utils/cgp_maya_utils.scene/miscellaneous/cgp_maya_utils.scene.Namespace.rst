Namespace
==============================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: Namespace
    :members:
    :undoc-members:
    :show-inheritance: