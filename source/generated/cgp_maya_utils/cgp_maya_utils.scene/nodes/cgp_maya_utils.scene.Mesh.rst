Mesh
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: Mesh
    :members:
    :undoc-members:
    :show-inheritance: