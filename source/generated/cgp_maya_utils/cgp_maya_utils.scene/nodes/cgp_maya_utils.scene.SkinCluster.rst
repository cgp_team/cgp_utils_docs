SkinCluster
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: SkinCluster
    :members:
    :undoc-members:
    :show-inheritance: