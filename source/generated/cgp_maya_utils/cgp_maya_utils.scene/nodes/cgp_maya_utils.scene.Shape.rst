Shape
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: Shape
    :members:
    :undoc-members:
    :show-inheritance: