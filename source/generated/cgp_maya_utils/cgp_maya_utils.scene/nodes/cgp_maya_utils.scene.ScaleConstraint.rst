ScaleConstraint
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: ScaleConstraint
    :members:
    :undoc-members:
    :show-inheritance: