AnimCurveTU
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: AnimCurveTU
    :members:
    :undoc-members:
    :show-inheritance: