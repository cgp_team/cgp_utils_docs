DagNode
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: DagNode
    :members:
    :undoc-members:
    :show-inheritance: