AnimCurve
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: AnimCurve
    :members:
    :undoc-members:
    :show-inheritance: