AnimCurveTA
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: AnimCurveTA
    :members:
    :undoc-members:
    :show-inheritance: