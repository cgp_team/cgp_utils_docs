Transform
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: Transform
    :members:
    :undoc-members:
    :show-inheritance: