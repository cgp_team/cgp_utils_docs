GeometryFilter
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: GeometryFilter
    :members:
    :undoc-members:
    :show-inheritance: