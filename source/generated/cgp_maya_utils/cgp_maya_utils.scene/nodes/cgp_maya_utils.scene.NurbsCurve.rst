NurbsCurve
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: NurbsCurve
    :members:
    :undoc-members:
    :show-inheritance: