AnimCurveTL
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: AnimCurveTL
    :members:
    :undoc-members:
    :show-inheritance: