Joint
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: Joint
    :members:
    :undoc-members:
    :show-inheritance: