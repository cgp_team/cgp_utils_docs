OrientConstraint
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: OrientConstraint
    :members:
    :undoc-members:
    :show-inheritance: