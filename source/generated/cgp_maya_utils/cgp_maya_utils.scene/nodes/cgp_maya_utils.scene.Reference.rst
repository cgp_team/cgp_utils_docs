Reference
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: Reference
    :members:
    :undoc-members:
    :show-inheritance: