Node
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: Node
    :members:
    :undoc-members:
    :show-inheritance: