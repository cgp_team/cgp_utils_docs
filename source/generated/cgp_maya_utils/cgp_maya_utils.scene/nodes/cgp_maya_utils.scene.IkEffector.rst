IkEffector
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: IkEffector
    :members:
    :undoc-members:
    :show-inheritance: