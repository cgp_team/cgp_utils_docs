Constraint
==========================

.. currentmodule::  cgp_maya_utils.scene

.. autoclass:: Constraint
    :members:
    :undoc-members:
    :show-inheritance: