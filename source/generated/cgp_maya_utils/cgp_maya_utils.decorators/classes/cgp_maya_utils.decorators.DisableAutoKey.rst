DisableAutoKey
========================================

.. currentmodule::  cgp_maya_utils.decorators

.. autoclass:: DisableAutoKey
    :members:
    :undoc-members:
    :show-inheritance: