NamespaceContext
==========================================

.. currentmodule::  cgp_maya_utils.decorators

.. autoclass:: NamespaceContext
    :members:
    :undoc-members:
    :show-inheritance: