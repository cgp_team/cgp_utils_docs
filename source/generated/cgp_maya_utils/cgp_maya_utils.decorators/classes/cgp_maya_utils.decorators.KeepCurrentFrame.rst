KeepCurrentFrame
==========================================

.. currentmodule::  cgp_maya_utils.decorators

.. autoclass:: KeepCurrentFrame
    :members:
    :undoc-members:
    :show-inheritance: