KeepCurrentFrameRange
===============================================

.. currentmodule::  cgp_maya_utils.decorators

.. autoclass:: KeepCurrentFrameRange
    :members:
    :undoc-members:
    :show-inheritance: