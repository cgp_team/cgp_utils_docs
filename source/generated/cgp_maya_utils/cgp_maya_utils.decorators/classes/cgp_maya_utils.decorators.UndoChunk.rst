UndoChunk
===================================

.. currentmodule::  cgp_maya_utils.decorators

.. autoclass:: UndoChunk
    :members:
    :undoc-members:
    :show-inheritance:
