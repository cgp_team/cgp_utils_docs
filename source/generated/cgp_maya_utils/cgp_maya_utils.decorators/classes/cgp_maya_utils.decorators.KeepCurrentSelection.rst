KeepCurrentSelection
==============================================

.. currentmodule::  cgp_maya_utils.decorators

.. autoclass:: KeepCurrentSelection
    :members:
    :undoc-members:
    :show-inheritance: