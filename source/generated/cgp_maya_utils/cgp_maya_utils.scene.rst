cgp\_maya\_utils.scene
======================

.. automodule:: cgp_maya_utils.scene


    .. rubric:: Classes ``connection``

    .. autosummary::
        :toctree: cgp_maya_utils.scene/attributes

            Connection


    .. rubric:: Classes ``attribute``

    .. autosummary::
        :toctree: cgp_maya_utils.scene/attributes

            Attribute
            BoolAttribute
            ByteAttribute
            CompoundAttribute
            Double3Attribute
            DoubleAngleAttribute
            DoubleAttribute
            DoubleLinearAttribute
            EnumAttribute
            Float3Attribute
            FloatAttribute
            LongAttribute
            MatrixAttribute
            MessageAttribute
            NumericAttribute
            ShortAttribute
            StringAttribute
            TDataCompoundAttribute
            TimeAttribute


    .. rubric:: Classes ``node``

    .. autosummary::
        :toctree: cgp_maya_utils.scene/nodes

            AimConstraint
            AnimCurve
            AnimCurveTA
            AnimCurveTL
            AnimCurveTU
            Constraint
            DagNode
            GeometryFilter
            IkEffector
            IkHandle
            Joint
            Mesh
            Node
            NurbsCurve
            NurbsSurface
            OrientConstraint
            ParentConstraint
            PointConstraint
            Reference
            ScaleConstraint
            Shape
            SkinCluster
            Transform


    .. rubric:: Classes ``component``

    .. autosummary::
        :toctree: cgp_maya_utils.scene/components

            Component
            CurvePoint
            Edge
            EditPoint
            Face
            IsoparmU
            IsoparmV
            SurfacePatch
            SurfacePoint
            TransformComponent
            Vertex


    .. rubric:: Classes ``miscellaneous``

    .. autosummary::
        :toctree: cgp_maya_utils.scene/miscellaneous

            Namespace
            Plugin
            Scene


    .. rubric:: Functions

    .. autosummary::
        :toctree: cgp_maya_utils.scene/functions

            attribute
            component
            connection
            createAttribute
            createNode
            currentNamespace
            getAttributes
            getNodes
            getNodesFromAttributes
            namespace
            node
            plugin
            scene
