AnimKey
==========================

.. warning:: ``AnimKey.data()`` gathering is incomplete. Tangent positions are missing.

.. currentmodule::  cgp_maya_utils.api

.. autoclass:: AnimKey
    :members:
    :undoc-members:
    :show-inheritance:
