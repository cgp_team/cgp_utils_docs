MayaObject
=============================

.. currentmodule::  cgp_maya_utils.api

.. autoclass:: MayaObject
    :members:
    :undoc-members:
    :show-inheritance:
