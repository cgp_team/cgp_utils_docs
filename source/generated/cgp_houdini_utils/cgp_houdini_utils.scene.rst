cgp\_houdini\_utils.scene
=========================

.. automodule:: cgp_houdini_utils.scene


    .. rubric:: Classes ``parm``

    .. autosummary::
        :toctree: cgp_houdini_utils.scene/parms

            Parm


    .. rubric:: Classes ``node``

    .. autosummary::
        :toctree: cgp_houdini_utils.scene/nodes

            Node


    .. rubric:: Classes ``miscellaneous``

    .. autosummary::
        :toctree: cgp_houdini_utils.scene/miscellaneous

            Scene


    .. rubric:: Functions

    .. autosummary::
        :toctree: cgp_houdini_utils.scene/functions

            node
            parm
