cgp\_houdini\_utils.files
=========================

.. automodule:: cgp_houdini_utils.files

    .. rubric:: Classes

    .. autosummary::
        :toctree: cgp_houdini_utils.files/classes

            HipFile
            HipncFile


    .. rubric:: Functions

    .. autosummary::
        :toctree: cgp_houdini_utils.files/functions

            registerFileTypes
