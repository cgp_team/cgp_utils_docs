RotateOrder
====================================

.. currentmodule::  cgp_houdini_utils.constants

.. autoclass:: RotateOrder
    :members:
    :undoc-members: