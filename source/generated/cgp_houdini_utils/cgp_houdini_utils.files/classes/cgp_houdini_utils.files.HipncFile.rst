MayaFile
=============================

.. currentmodule::  cgp_houdini_utils.files

.. autoclass:: HipncFile
    :members:
    :undoc-members:
    :show-inheritance: