MaFile
===========================

.. currentmodule::  cgp_houdini_utils.files

.. autoclass:: HipFile
    :members:
    :undoc-members:
    :show-inheritance:
