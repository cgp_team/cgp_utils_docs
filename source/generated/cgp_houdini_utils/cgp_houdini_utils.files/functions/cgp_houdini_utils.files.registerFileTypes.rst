registerFileTypes
======================================

.. currentmodule:: cgp_houdini_utils.files

.. autofunction:: registerFileTypes
