Node
==========================

.. currentmodule::  cgp_houdini_utils.scene

.. autoclass:: Node
    :members:
    :undoc-members:
    :show-inheritance: