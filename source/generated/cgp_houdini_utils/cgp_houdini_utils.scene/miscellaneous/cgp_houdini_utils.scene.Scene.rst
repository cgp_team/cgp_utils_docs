Scene
==========================

.. currentmodule::  cgp_houdini_utils.scene

.. autoclass:: Scene
    :members:
    :undoc-members:
    :show-inheritance: