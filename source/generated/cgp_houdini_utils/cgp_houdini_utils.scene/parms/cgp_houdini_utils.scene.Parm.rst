Parm
==========================

.. currentmodule::  cgp_houdini_utils.scene

.. autoclass:: Parm
    :members:
    :undoc-members:
    :show-inheritance: