CollapsibleWidget
=================

.. currentmodule::  cgp_generic_utils.qt

.. autoclass:: CollapsibleWidget
    :members:
    :undoc-members:
    :show-inheritance: