Tool
====

.. currentmodule::  cgp_generic_utils.qt

.. autoclass:: Tool
    :members:
    :undoc-members:
    :show-inheritance: