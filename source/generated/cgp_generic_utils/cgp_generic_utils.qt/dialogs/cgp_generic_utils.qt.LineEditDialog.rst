LineEditDialog
==============

.. currentmodule::  cgp_generic_utils.qt

.. autoclass:: LineEditDialog
    :members:
    :undoc-members:
    :show-inheritance: