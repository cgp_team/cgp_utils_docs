TextEditDialog
==============

.. currentmodule::  cgp_generic_utils.qt

.. autoclass:: TextEditDialog
    :members:
    :undoc-members:
    :show-inheritance: