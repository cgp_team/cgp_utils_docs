ComboBoxLineEditDialog
======================

.. currentmodule::  cgp_generic_utils.qt

.. autoclass:: ComboBoxLineEditDialog
    :members:
    :undoc-members:
    :show-inheritance: