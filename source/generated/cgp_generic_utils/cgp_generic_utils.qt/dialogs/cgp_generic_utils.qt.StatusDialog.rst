StatusDialog
============

.. currentmodule::  cgp_generic_utils.qt

.. autoclass:: StatusDialog
    :members:
    :undoc-members:
    :show-inheritance: