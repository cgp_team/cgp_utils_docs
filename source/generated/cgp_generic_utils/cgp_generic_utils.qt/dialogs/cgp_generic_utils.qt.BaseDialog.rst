BaseDialog
==========

.. currentmodule::  cgp_generic_utils.qt

.. autoclass:: BaseDialog
    :members:
    :undoc-members:
    :show-inheritance: