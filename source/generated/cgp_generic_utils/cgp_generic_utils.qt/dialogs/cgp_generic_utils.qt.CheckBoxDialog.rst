CheckBoxDialog
==============

.. currentmodule::  cgp_generic_utils.qt

.. autoclass:: CheckBoxDialog
    :members:
    :undoc-members:
    :show-inheritance: