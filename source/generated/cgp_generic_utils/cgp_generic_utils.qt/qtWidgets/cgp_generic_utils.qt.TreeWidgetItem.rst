TreeWidgetItem
==============

.. currentmodule::  cgp_generic_utils.qt

.. autoclass:: TreeWidgetItem
    :members:
    :undoc-members:
    :show-inheritance: