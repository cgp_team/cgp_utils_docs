LineEdit
========

.. currentmodule::  cgp_generic_utils.qt

.. autoclass:: LineEdit
    :members:
    :undoc-members:
    :show-inheritance: