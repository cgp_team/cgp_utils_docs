TreeWidget
==========

.. currentmodule::  cgp_generic_utils.qt

.. autoclass:: TreeWidget
    :members:
    :undoc-members:
    :show-inheritance: