PushButton
==========

.. currentmodule::  cgp_generic_utils.qt

.. autoclass:: PushButton
    :members:
    :undoc-members:
    :show-inheritance: