ListWidget
==========

.. currentmodule::  cgp_generic_utils.qt

.. autoclass:: ListWidget
    :members:
    :undoc-members:
    :show-inheritance: