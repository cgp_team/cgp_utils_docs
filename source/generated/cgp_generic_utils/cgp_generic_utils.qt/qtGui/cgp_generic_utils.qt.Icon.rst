Icon
====

.. currentmodule::  cgp_generic_utils.qt

.. autoclass:: Icon
    :members:
    :undoc-members:
    :show-inheritance: