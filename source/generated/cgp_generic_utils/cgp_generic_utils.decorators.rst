cgp\_generic\_utils.decorators
==============================

.. automodule:: cgp_generic_utils.decorators


    .. rubric:: Classes

    .. autosummary::
        :toctree: cgp_generic_utils.decorators/classes

            Decorator
            Profiler
            StatusDialog
            Timer
