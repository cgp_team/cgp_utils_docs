UiFile
======================================

.. currentmodule::  cgp_generic_utils.files

.. autoclass:: UiFile
    :members:
    :undoc-members:
    :show-inheritance: