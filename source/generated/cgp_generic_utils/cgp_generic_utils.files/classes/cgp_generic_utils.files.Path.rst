Path
======================================

.. currentmodule::  cgp_generic_utils.files

.. autoclass:: Path
    :members:
    :undoc-members:
    :show-inheritance: