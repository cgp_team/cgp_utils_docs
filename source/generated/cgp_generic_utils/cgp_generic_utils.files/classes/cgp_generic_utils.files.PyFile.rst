PyFile
======================================

.. currentmodule::  cgp_generic_utils.files

.. autoclass:: PyFile
    :members:
    :undoc-members:
    :show-inheritance: