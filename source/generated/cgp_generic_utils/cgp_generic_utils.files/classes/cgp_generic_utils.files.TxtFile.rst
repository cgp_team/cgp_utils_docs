TxtFile
======================================

.. currentmodule::  cgp_generic_utils.files

.. autoclass:: TxtFile
    :members:
    :undoc-members:
    :show-inheritance: