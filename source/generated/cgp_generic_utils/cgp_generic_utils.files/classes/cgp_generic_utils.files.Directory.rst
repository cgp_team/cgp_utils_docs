Directory
======================================

.. currentmodule::  cgp_generic_utils.files

.. autoclass:: Directory
    :members:
    :undoc-members:
    :show-inheritance: