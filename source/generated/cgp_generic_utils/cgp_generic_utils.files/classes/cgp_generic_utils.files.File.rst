File
======================================

.. currentmodule::  cgp_generic_utils.files

.. autoclass:: File
    :members:
    :undoc-members:
    :show-inheritance: