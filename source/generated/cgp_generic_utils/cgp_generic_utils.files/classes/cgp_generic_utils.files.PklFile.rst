PklFile
======================================

.. currentmodule::  cgp_generic_utils.files

.. autoclass:: PklFile
    :members:
    :undoc-members:
    :show-inheritance: