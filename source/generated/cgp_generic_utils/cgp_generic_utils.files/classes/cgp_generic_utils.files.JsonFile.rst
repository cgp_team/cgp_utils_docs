JsonFile
======================================

.. currentmodule::  cgp_generic_utils.files

.. autoclass:: JsonFile
    :members:
    :undoc-members:
    :show-inheritance: