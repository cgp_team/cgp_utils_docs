createDirectory
=============================

.. currentmodule:: cgp_generic_utils.files

.. autofunction:: createDirectory
