registerFileTypes
=============================

.. currentmodule:: cgp_generic_utils.files

.. autofunction:: registerFileTypes
