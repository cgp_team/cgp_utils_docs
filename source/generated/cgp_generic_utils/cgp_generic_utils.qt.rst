cgp\_generic\_utils.qt
======================

.. automodule:: cgp_generic_utils.qt

    .. rubric:: Classes ``Subclassed QtWidgets``

    .. autosummary::
        :toctree: cgp_generic_utils.qt/qtWidgets

            ComboBox
            LineEdit
            ListWidget
            PushButton
            TreeWidget
            TreeWidgetItem

    .. rubric:: Classes ``Subclassed QtGui``

    .. autosummary::
        :toctree: cgp_generic_utils.qt/qtGui

            Font
            Icon

    .. rubric:: Classes ``Custom Widgets``

    .. autosummary::
        :toctree: cgp_generic_utils.qt/customWidgets

            CollapsibleWidget
            Tool

    .. rubric:: Classes ``Dialogs``

    .. autosummary::
        :toctree: cgp_generic_utils.qt/dialogs

            BaseDialog
            CheckBoxDialog
            ComboBoxDialog
            ComboBoxLineEditDialog
            LineEditDialog
            StatusDialog
            TextEditDialog
