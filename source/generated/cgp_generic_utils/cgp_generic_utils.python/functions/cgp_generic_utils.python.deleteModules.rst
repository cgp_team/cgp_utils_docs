deleteModules
==================================

.. currentmodule:: cgp_generic_utils.python

.. autofunction:: deleteModules
