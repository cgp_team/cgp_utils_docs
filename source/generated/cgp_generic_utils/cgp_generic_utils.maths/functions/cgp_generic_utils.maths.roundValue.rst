roundValue
==================================

.. currentmodule:: cgp_generic_utils.maths

.. autofunction:: roundValue
