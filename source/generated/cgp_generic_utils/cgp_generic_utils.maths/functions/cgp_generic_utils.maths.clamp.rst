clamp
=============================

.. currentmodule:: cgp_generic_utils.maths

.. autofunction:: clamp
