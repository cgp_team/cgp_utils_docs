cgp\_generic\_utils.maths
=========================

.. automodule:: cgp_generic_utils.maths


    .. rubric:: Functions

    .. autosummary::
        :toctree: cgp_generic_utils.maths/functions

            clamp
            roundValue
