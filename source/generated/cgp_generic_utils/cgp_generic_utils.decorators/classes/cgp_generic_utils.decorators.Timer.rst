Timer
======================================

.. currentmodule::  cgp_generic_utils.decorators

.. autoclass:: Timer
    :members:
    :undoc-members:
    :show-inheritance: