Profiler
======================================

.. currentmodule::  cgp_generic_utils.decorators

.. autoclass:: Profiler
    :members:
    :undoc-members:
    :show-inheritance: