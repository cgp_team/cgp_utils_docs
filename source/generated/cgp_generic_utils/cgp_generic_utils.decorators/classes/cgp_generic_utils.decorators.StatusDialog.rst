StatusDialog
======================================

.. currentmodule::  cgp_generic_utils.decorators

.. autoclass:: StatusDialog
    :members:
    :undoc-members:
    :show-inheritance: