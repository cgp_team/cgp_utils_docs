Decorator
======================================

.. currentmodule::  cgp_generic_utils.decorators

.. autoclass:: Decorator
    :members:
    :undoc-members:
    :show-inheritance: