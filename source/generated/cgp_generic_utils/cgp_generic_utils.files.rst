cgp\_generic\_utils.files
=========================

.. automodule:: cgp_generic_utils.files

    .. rubric:: Classes

    .. autosummary::
        :toctree: cgp_generic_utils.files/classes

            Directory
            File
            JsonFile
            Path
            PklFile
            PyFile
            TxtFile
            UiFile

    .. rubric:: Functions

    .. autosummary::
        :toctree: cgp_generic_utils.files/functions

            createDirectory
            createFile
            entity
            registerFileTypes
