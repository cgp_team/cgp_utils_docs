FileExtension
======================================

.. currentmodule::  cgp_generic_utils.constants

.. autoclass:: FileExtension
    :members:
    :undoc-members: