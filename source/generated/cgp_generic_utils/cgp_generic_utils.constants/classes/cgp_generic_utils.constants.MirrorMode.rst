MirrorMode
======================================

.. currentmodule::  cgp_generic_utils.constants

.. autoclass:: MirrorMode
    :members:
    :undoc-members: