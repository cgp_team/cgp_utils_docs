Orientation
======================================

.. currentmodule::  cgp_generic_utils.constants

.. autoclass:: Orientation
    :members:
    :undoc-members: