TypoStyle
======================================

.. currentmodule::  cgp_generic_utils.constants

.. autoclass:: TypoStyle
    :members:
    :undoc-members: