LogType
======================================

.. currentmodule::  cgp_generic_utils.constants

.. autoclass:: LogType
    :members:
    :undoc-members: