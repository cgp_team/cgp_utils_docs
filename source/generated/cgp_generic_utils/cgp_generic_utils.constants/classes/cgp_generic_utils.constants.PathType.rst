PathType
======================================

.. currentmodule::  cgp_generic_utils.constants

.. autoclass:: PathType
    :members:
    :undoc-members: