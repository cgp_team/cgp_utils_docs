Axis
======================================

.. currentmodule::  cgp_generic_utils.constants

.. autoclass:: Axis
    :members:
    :undoc-members: