AxisTable
======================================

.. currentmodule::  cgp_generic_utils.constants

.. autoclass:: AxisTable
    :members:
    :undoc-members: