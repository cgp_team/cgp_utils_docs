TransformMode
======================================

.. currentmodule::  cgp_generic_utils.constants

.. autoclass:: TransformMode
    :members:
    :undoc-members: