Environment
======================================

.. currentmodule::  cgp_generic_utils.constants

.. autoclass:: Environment
    :members:
    :undoc-members: