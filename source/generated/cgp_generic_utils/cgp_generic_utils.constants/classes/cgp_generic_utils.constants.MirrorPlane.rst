MirrorPlane
======================================

.. currentmodule::  cgp_generic_utils.constants

.. autoclass:: MirrorPlane
    :members:
    :undoc-members: