Side
======================================

.. currentmodule::  cgp_generic_utils.constants

.. autoclass:: Side
    :members:
    :undoc-members: