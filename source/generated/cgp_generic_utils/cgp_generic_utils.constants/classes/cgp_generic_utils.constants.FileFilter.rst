FileFilter
======================================

.. currentmodule::  cgp_generic_utils.constants

.. autoclass:: FileFilter
    :members:
    :undoc-members: