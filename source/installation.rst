============
Installation
============

| CGP utils is certified to work in production on Windows and Linux environments only.
| Package may also work on OSX but have never been actually tested in this environment.

-----------------
cgp_generic_utils
-----------------

repository :
    https://github.com/cgpilou/cgp_generic_utils

requirements :
    * ``PySide2``
    * ``Python-2.7``

install for maya :
    #. clone the repository and save it on your network
    #. create ``module`` directory in the maya preferences if not already existing
    #. copy/paste the ``cgp_generic_utils.mod`` file to the ``module`` directory
    #. update the path to the repository in ``cgp_generic_utils.mod``

install for houdini :
    #. clone the repository and save it on your network
    #. copy/paste the content of the ``houdini.env`` file in the ``houdini.env`` file of the installation
    #. update the ``CGP_GENERIC_UTILS_ROOT`` variable with the path to the repository in the ``houdini.env`` file of the installation
    #. edit ``cgp_generic_utils.constants.Environment`` variables with the infos relative to your environment

--------------
cgp_maya_utils
--------------

repository :
    https://github.com/cgpilou/cgp_maya_utils

requirements :
    * ``Python-2.7``
    * ``maya-2018+``
    * ``cgp_generic_utils``

install :
    #. clone the repository and save it on your network
    #. create ``module`` directory in the maya preferences if not already existing
    #. copy/paste the ``cgp_maya_utils.mod`` file to the ``module`` directory
    #. update the path to the repository in ``cgp_maya_utils.mod``
    #. edit ``cgp_maya_utils.constants.Environment`` variables with the infos relative to your environment


-----------------
cgp_houdini_utils
-----------------

repository :
    https://github.com/cgpilou/cgp_houdini_utils

requirements :
    * ``Python-2.7``
    * ``houdini-18+``
    * ``cgp_generic_utils``

install for houdini :
    #. clone the repository and save it on your network
    #. copy/paste the content of the ``houdini.env`` file in the ``houdini.env`` file of the installation
    #. update the ``CGP_HOUDINI_UTILS_ROOT`` variable with the path to the repository in the ``houdini.env`` file of the installation
    #. edit ``cgp_houdini_utils.constants.Environment`` variables with the infos relative to your environment
