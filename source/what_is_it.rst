============
What is it ?
============

**CGP utils** is a python API that comes in three parts :

   * ``cgp_generic_utils``
   * ``cgp_maya_utils``
   * ``cgp_houdini_utils``

It allows a wide range of possibilities such as :

   * manipulations inside Maya and Houdini scenes
   * UI advanced and easy use functionalities
   * python decorators and contexts for easy script encapsulation
   * files management
   * much more ...

``cgp_generic_utils``
   python API agnostic of any specific DCC, that provides generic functionalities such as :

      * generic constants
      * generic decorators
      * files classes and management functions
      * mathematical functions
      * qt library for easier UI development

``cgp_maya_utils``
   python API for Maya that homogenizes and simplifies the commands offered by Maya's built-in libraries.
   It provides classes and functions such as :

      * maya constants
      * maya decorators
      * subclassed OpenMaya objects with custom functionalities
      * maya classes for nodes, attributes, components, plugins and more

``cgp_houdini_utils``
   python API for houdini that wraps a few useful functionalities over an already well houdini built-in API.
   It provides classes and functions such as :

      * houdini constants
      * houdini classes for nodes and parameters for easy subclassing and custom functionalities implementation and more
