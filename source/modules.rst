=======================
Technical Documentation
=======================

---------------------
``cgp_generic_utils``
---------------------

.. autosummary::
    :nosignatures:
    :toctree: generated/cgp_generic_utils
  
        cgp_generic_utils.constants
        cgp_generic_utils.decorators
        cgp_generic_utils.files
        cgp_generic_utils.maths
        cgp_generic_utils.python
        cgp_generic_utils.qt

------------------
``cgp_maya_utils``
------------------

.. autosummary::
    :nosignatures:
    :toctree: generated/cgp_maya_utils

        cgp_maya_utils.api
        cgp_maya_utils.constants
        cgp_maya_utils.decorators
        cgp_maya_utils.files
        cgp_maya_utils.scene

---------------------
``cgp_houdini_utils``
---------------------

.. autosummary::
    :nosignatures:
    :toctree: generated/cgp_houdini_utils

        cgp_houdini_utils.constants
        cgp_houdini_utils.files
        cgp_houdini_utils.scene
