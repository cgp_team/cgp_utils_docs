
## Build documentation

* go to documentation folder `cd doc`
* launch sphinx quick start `sphinx-quickstart`
* update `conf.py` and `index.rst`
* generate the modules rst files `sphinx-apidoc -o source ../lib`
* generate the documentation `make html`